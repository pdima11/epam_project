CREATE DATABASE testing_system;

USE testing_system;

CREATE TABLE user_roles(
	role_id INT NOT NULL AUTO_INCREMENT,
    role VARCHAR(20),
	CONSTRAINT PK_USER_ROLES PRIMARY KEY(role_id)
);

CREATE TABLE users(
	user_id INT NOT NULL AUTO_INCREMENT,
    login VARCHAR(30),
    email VARCHAR(40),
    password VARCHAR(50),
    role_id INT DEFAULT 1,
    CONSTRAINT PK_USERS PRIMARY KEY(user_id),
    CONSTRAINT FK_USERS_USER_ROLES FOREIGN KEY(role_id) REFERENCES user_roles(role_id)
);

CREATE TABLE categories(
	category_id INT NOT NULL AUTO_INCREMENT,
    category_name VARCHAR(20) NOT NULL,
    CONSTRAINT PK_CATEGORIES PRIMARY KEY(category_id)
);

CREATE TABLE tests(
	test_id INT NOT NULL AUTO_INCREMENT,
    test_name VARCHAR(50) NOT NULL,
    category_id INT NOT NULL,
    CONSTRAINT PK_TESTS PRIMARY KEY(test_id),
    CONSTRAINT FK_TESTS_CATEGORIES FOREIGN KEY(category_id) REFERENCES categories(category_id)
);

CREATE TABLE questions(
	question_id INT NOT NULL AUTO_INCREMENT,
    question VARCHAR(250) NOT NULL,
    test_id INT NOT NULL,
    CONSTRAINT PK_QUESTIONS PRIMARY KEY(question_id),
    CONSTRAINT FK_QUESTIONS_TESTS FOREIGN KEY(test_id) REFERENCES tests(test_id)
);

CREATE TABLE answers(
	answer_id INT NOT NULL AUTO_INCREMENT,
    answer VARCHAR(150) NOT NULL,
    correct BOOLEAN  NOT NULL,
    question_id INT NOT NULL,
    CONSTRAINT PK_ANSWERS PRIMARY KEY(answer_id),
    CONSTRAINT FK_ANSWERS_QUESTIONS FOREIGN KEY(question_id) REFERENCES questions(question_id)
);

INSERT INTO categories(category_name)
VALUES ('Geography');

INSERT INTO tests(test_name, category_id)
VALUES ('Capitals of Europe', 
        (SELECT category_id
         FROM categories
		 WHERE category_name = 'Geography')
       );

INSERT INTO questions(question, test_id)
VALUES ('What is the capital city of France',
		(SELECT test_id 
         FROM tests
		 WHERE test_name = 'Capitals of Europe')
	   ),
       ('What is the capital city of England',
		(SELECT test_id 
         FROM tests
		 WHERE test_name = 'Capitals of Europe')
	   );

INSERT INTO questions(question, once_correct_answer, test_id)
VALUES ('What is not the capital city of Spain', FALSE,
		(SELECT test_id 
         FROM tests
		 WHERE test_name = 'Capitals of Europe')
	   );

UPDATE questions
SET once_correct_answer = TRUE
WHERE question_id < 4;



SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of France';

INSERT INTO answers(answer, correct, question_id)
VALUES ('Madrid', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is not the capital city of Spain')
	   ),
       ('Valencia', TRUE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is not the capital city of Spain')
	   ),
       ('Lisbon', TRUE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is not the capital city of Spain')
	   ),
       ('Barcelona', TRUE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is not the capital city of Spain')
	   );


INSERT INTO answers(answer, correct, question_id)
VALUES ('Paris', TRUE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of France')
	   ),
       ('Berlin', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of France')
	   ),
       ('London', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of France')
	   ),
       ('Tokyo', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of France')
	   ),
       ('Madrid', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of England')
	   ),
       ('Rome', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of England')
	   ),
       ('Lisbon', FALSE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of England')
	   ),
       ('London', TRUE,
		(SELECT question_id 
         FROM questions
		 WHERE question = 'What is the capital city of England')
	   );

SELECT * FROM answers;
SELECT * FROM questions;
SELECT * FROM tests;
SELECT * FROM users;

SELECT test_id 
FROM tests 
WHERE date_added = (SELECT MAX(date_added) FROM tests WHERE test_name = 'AB')

INSERT INTO tests(group_update_id, test_name, category_id) VALUES ((SELECT `AUTO_INCREMENT`
																	FROM INFORMATION_SCHEMA.TABLES
																	WHERE TABLE_NAME = 'tests'), 'b', 1);
UPDATE tests
SET group_update_id = 1
WHERE test_id = 1; 

INSERT INTO tests(group_update_id, test_name, category_id) VALUES (1, 'Capitals of Europe', 1);

SELECT * FROM tests;

UPDATE tests SET date_added = date_added - 3 WHERE test_id = 6;

SELECT tests.test_id, group_update_id, test_name, category_name, COUNT(questions.test_id) AS questions_number
FROM categories, tests, questions 
WHERE categories.category_id = tests.category_id 
AND tests.test_id = questions.test_id
GROUP BY group_update_id
HAVING date_added = MAX(date_added);

SELECT tests.test_id, group_update_id, test_name, category_name, COUNT(questions.test_id) AS questions_number, date_added
FROM categories, tests, questions 
WHERE categories.category_id = tests.category_id 
AND tests.test_id = questions.test_id
AND date_added IN (SELECT MAX(date_added) FROM tests GROUP BY group_update_id)
GROUP BY group_update_id;

SELECT tests.test_id, group_update_id, test_name, category_name, COUNT(questions.test_id) AS questions_number 
FROM categories, tests, questions 
WHERE categories.category_id = tests.category_id 
AND tests.test_id = questions.test_id 
GROUP BY tests.test_id;

SELECT test_id, group_update_id, test_name, date_added
FROM tests
WHERE date_added IN (SELECT MAX(date_added) FROM tests GROUP BY group_update_id)
GROUP BY group_update_id;
                                                                    
INSERT INTO questions(question, one_correct_answer, test_id) VALUES (?, ?, ?);

INSERT INTO tests(test_name, category_id) VALUES ('test', 'Geography');
INSERT INTO questions(question, one_correct_answer, test_id) VALUES (?, ?, LAST_INSERT_ID());


ALTER TABLE questions CHANGE once_correct_answer one_correct_answer BOOLEAN NOT NULL;

ALTER TABLE tests
ADD group_update_id INT NOT NULL;

ALTER TABLE tests
ADD date_added DATETIME NOT NULL DEFAULT NOW();

ALTER TABLE questions
ADD once_correct_answer BOOLEAN NOT NULL;


DROP TABLE tests;
DROP TABLE questions;
DROP TABLE answers;

SELECT COUNT(test_id) AS tests_number FROM tests WHERE date_added IN (SELECT MAX(date_added) FROM tests GROUP BY group_update_id);

SELECT tests.test_id, test_name, category_name, COUNT(questions.test_id) AS questions_number
FROM categories, tests, questions
WHERE categories.category_id = tests.category_id
AND tests.test_id = questions.test_id
AND tests.test_id = 1;

SELECT COUNT(user_id) FROM users;

SELECT user_id, login, password, email, role
FROM users, user_roles
WHERE users.role_id = user_roles.role_id 
LIMIT 4, 8;

SELECT user_id, login, password(password), email, role
FROM users, user_roles
WHERE users.role_id = user_roles.role_id 
AND BINARY login = 'admin'
AND password = password('admin11');

UPDATE users
SET role_id = (SELECT role_id FROM user_roles WHERE role = 'User')
WHERE user_id = 21;

UPDATE users
SET role_id = (SELECT role_id FROM user_roles WHERE role = 'Admin')
WHERE user_id = 1;

SELECT COUNT(test_id) 
FROM questions
WHERE test_id = 1; 

DELETE answers, questions, tests	
FROM answers, questions, tests
WHERE answers.question_id = questions.question_id
AND questions.test_id = tests.test_id
AND tests.test_id = 3;

DELETE 	
FROM tests
WHERE  tests.test_id = 34;

ALTER TABLE answers 
MODIFY question_id INT NOT NULL ON DELETE CASCADE; 

ALTER TABLE answers DROP FOREIGN KEY FK_ANSWERS_QUESTIONS;

ALTER TABLE answers
ADD CONSTRAINT FK_ANSWERS_QUESTIONS 
FOREIGN KEY(question_id) REFERENCES questions(question_id)
ON DELETE CASCADE;

ALTER TABLE questions DROP FOREIGN KEY FK_QUESTIONS_TESTS;

ALTER TABLE questions
ADD CONSTRAINT FK_QUESTIONS_TESTS 
FOREIGN KEY(test_id) REFERENCES tests(test_id)
ON DELETE CASCADE;

INSERT INTO user_roles(role)
VALUES ('User'),
	   ('Admin'),
       ('Tutor');

INSERT INTO users(login, password, email, role_id)
VALUES ('tutor', password('tutor11'), 'tutor@yandex.ru', (SELECT role_id FROM user_roles WHERE role='Tutor'));

INSERT INTO users(login, password, email)
VALUES ('fff', password('123456'), 'ythtyerman@yandex.ru'),
	   ('ggg', password('123456'), 'referfan@yandex.ru'),
       ('aaa', password('123456'), 'tgrerman@yandex.ru'),
	   ('bbb', password('123456'), 'tgrrman@yandex.ru'),
	   ('ccc', password('123456'), 'trgtrgtrerman@yandex.ru'),
       ('ddd', password('123456'), 'dyt@yandex.ru'),
	   ('eee', password('123456'), 'ythtyan@yandex.ru'),;

SELECT * FROM users;

 SELECT tests.test_id, test_name, category_name
 FROM categories, tests 
 WHERE categories.category_id = tests.category_id ;

 SELECT tests.test_id, test_name, category_name, COUNT(questions.question_id) AS questions_number 
 FROM tests, categories, questions 
 WHERE tests.category_id = categories.category_id 
 AND tests.test_id = questions.test_id 
 GROUP BY tests.test_id
LIMIT ?, ?;

DROP DATABASE testing_system;