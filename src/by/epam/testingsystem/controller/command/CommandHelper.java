package by.epam.testingsystem.controller.command;

import by.epam.testingsystem.controller.command.impl.admin.ChangeUserRoleCommand;
import by.epam.testingsystem.controller.command.impl.admin.DeleteTestCommand;
import by.epam.testingsystem.controller.command.impl.admin.DeleteUserCommand;
import by.epam.testingsystem.controller.command.impl.admin.LoadUserListCommand;
import by.epam.testingsystem.controller.command.impl.tutor.AddTestCommand;
import by.epam.testingsystem.controller.command.impl.LoadTestListCommand;
import by.epam.testingsystem.controller.command.impl.test.StartTestCommand;
import by.epam.testingsystem.controller.command.impl.test.TestResultCommand;
import by.epam.testingsystem.controller.command.impl.tutor.ModificationTestCommand;
import by.epam.testingsystem.controller.command.impl.LanguageCommand;
import by.epam.testingsystem.controller.command.impl.user.LoginCommand;
import by.epam.testingsystem.controller.command.impl.user.LogoutCommand;
import by.epam.testingsystem.controller.command.impl.user.SignUpCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 30.11.2015.
 */
public class CommandHelper {
    private Map<CommandType, Command> commands = new HashMap<>();

    public CommandHelper() {
        commands.put(CommandType.LOG_IN, new LoginCommand());
        commands.put(CommandType.LOG_OUT, new LogoutCommand());
        commands.put(CommandType.REGISTER, new SignUpCommand());
        commands.put(CommandType.CHANGE_LANGUAGE, new LanguageCommand());
        commands.put(CommandType.LOAD_USER_LIST, new LoadUserListCommand());
        commands.put(CommandType.DELETE_USER, new DeleteUserCommand());
        commands.put(CommandType.LOAD_TEST_LIST, new LoadTestListCommand());
        commands.put(CommandType.START_TEST, new StartTestCommand());
        commands.put(CommandType.TEST_RESULT, new TestResultCommand());
        commands.put(CommandType.ADD_TEST, new AddTestCommand());
        commands.put(CommandType.DELETE_TEST, new DeleteTestCommand());
        commands.put(CommandType.CHANGE_USER_ROLE, new ChangeUserRoleCommand());
        commands.put(CommandType.MODIFICATION_TEST, new ModificationTestCommand());
    }

    public Command getCommand(String commandName) {
        CommandType type = CommandType.getCommandName(commandName);
        return commands.get(type);
    }
}
