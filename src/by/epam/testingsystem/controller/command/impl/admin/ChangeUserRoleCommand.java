package by.epam.testingsystem.controller.command.impl.admin;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.ChangeUserRoleService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.impl.ChangeUserRoleServiceImpl;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by User on 23.11.2015.
 */
public class ChangeUserRoleCommand implements Command {
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String USER_ID_PARAMETER = "user-id";
    private static final String NEW_USER_ROLE_PARAMETER = "role";
    private static final String FORWARD_PAGE_CHANGE_USER_ROLE = "forward.page.load.users";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String SUCCESSFUL_CHANGE_USER_ROLE = "roleChanged";
    private static final String USER_PARAMETER = "user";
    private static final String ROLE_PARAMETER = "Admin";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = Resource.valueOf(FORWARD_PAGE_CHANGE_USER_ROLE);

        HttpSession session = request.getSession(false);

        if (session == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        User currentUser = (User) session.getAttribute(USER_PARAMETER);
        if (currentUser == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        if (!ROLE_PARAMETER.equals(currentUser.getRole())) {
            return Resource.valueOf(FORWARD_PAGE_HOME);
        }

            try {
            String userIdParameter = request.getParameter(USER_ID_PARAMETER);
            boolean isUserIdParameterInteger = RequestParameterValidator.isInteger(userIdParameter);
            boolean changeUserRole = false;

            if (isUserIdParameterInteger) {
                int userId = Integer.valueOf(userIdParameter);
                ChangeUserRoleService changeUserRoleService = ChangeUserRoleServiceImpl.getInstance();

                String role = request.getParameter(NEW_USER_ROLE_PARAMETER);

                changeUserRole = changeUserRoleService.changeRole(userId, role);
            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);
            }

            if (changeUserRole){
                request.setAttribute(SUCCESSFUL_CHANGE_USER_ROLE, true);
            } else {
                request.setAttribute(SUCCESSFUL_CHANGE_USER_ROLE, false);
            }

            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing ChangeUserRoleService", e);
        }
    }

}
