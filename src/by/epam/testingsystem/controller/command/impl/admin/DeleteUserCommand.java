package by.epam.testingsystem.controller.command.impl.admin;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.DeleteUserService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.impl.DeleteUserServiceImpl;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 11.11.2015.
 */
public class DeleteUserCommand implements Command {
    private static final String USER_ID_PARAMETER = "user-id";
    private static final String FORWARD_PAGE_DELETE_USER = "forward.page.load.users";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String SUCCESSFUL_DELETE_USER = "userRemoved";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        String forwardPage = Resource.valueOf(FORWARD_PAGE_DELETE_USER);

        try {
            String userIdParameter = request.getParameter(USER_ID_PARAMETER);
            boolean userDeleted = false;
            boolean isUserIdParameterInteger = RequestParameterValidator.isInteger(userIdParameter);

            if (isUserIdParameterInteger) {
                int userId = Integer.valueOf(userIdParameter);
                DeleteUserService deleteUserService = DeleteUserServiceImpl.getInstance();
                userDeleted = deleteUserService.deleteUser(userId);
            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);
            }

            if (userDeleted){
                request.setAttribute(SUCCESSFUL_DELETE_USER, true);
            } else {
                request.setAttribute(SUCCESSFUL_DELETE_USER, false);
            }

            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing DeleteUserService", e);
        }
    }
}
