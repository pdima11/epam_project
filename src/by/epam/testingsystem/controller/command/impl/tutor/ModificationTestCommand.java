package by.epam.testingsystem.controller.command.impl.tutor;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.StartTestService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.impl.QuestionListServiceImpl;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by User on 28.11.2015.
 */
public class ModificationTestCommand implements Command {
    private static final String FORWARD_PAGE_MODIFICATION_TEST = "forward.page.test.modification";
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String TEST_ID_PARAMETER = "test-id";
    private static final String TEST_PARAMETER = "test";
    private static final String USER_PARAMETER = "user";
    private static final String ROLE_PARAMETER = "Tutor";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = Resource.valueOf(FORWARD_PAGE_MODIFICATION_TEST);

        HttpSession session = request.getSession(false);

        if (session == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        User currentUser = (User) session.getAttribute(USER_PARAMETER);
        if (currentUser == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        if (!ROLE_PARAMETER.equals(currentUser.getRole())) {
            return Resource.valueOf(FORWARD_PAGE_HOME);
        }

        try {
            String testIdParameter = request.getParameter(TEST_ID_PARAMETER);
            boolean isTestIdParameterInteger = RequestParameterValidator.isInteger(testIdParameter);

            if (isTestIdParameterInteger) {
                int testId = Integer.valueOf(testIdParameter);

                StartTestService testService = QuestionListServiceImpl.getInstance();

                Test test = testService.getTest(testId);
                request.setAttribute(TEST_PARAMETER, test);
            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);
            }
            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing LoginServiceImpl", e);
        }
    }
}
