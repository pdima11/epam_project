package by.epam.testingsystem.controller.command.impl;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.impl.TestListServiceImpl;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class LoadTestListCommand implements Command {
    private static final String FORWARD_PAGE_TEST_LIST = "forward.page.test.list";
    private static final String TEST_LIST_PARAMETER = "tests";
    private static final String CURRENT_PAGE_PARAMETER = "currentPage";
    private static final String PAGES_NUMBER_PARAMETER = "pagesNumber";
    private static final String PAGE_PARAMETER = "page";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = Resource.valueOf(FORWARD_PAGE_TEST_LIST);

        int page = 1;
        String chosenPageParameter = request.getParameter(PAGE_PARAMETER);
        boolean isChosenPageParameterInteger = RequestParameterValidator.isInteger(chosenPageParameter);

        if (isChosenPageParameterInteger) {
            page = Integer.valueOf(chosenPageParameter);
        }

        TestListServiceImpl testListService = TestListServiceImpl.getInstance();
        try {
            List<Test> tests = testListService.getTestList(page);
            int pagesNumber = testListService.getPagesNumber();

            request.setAttribute(TEST_LIST_PARAMETER, tests);
            request.setAttribute(CURRENT_PAGE_PARAMETER, page);
            request.setAttribute(PAGES_NUMBER_PARAMETER, pagesNumber);

            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing LoginServiceImpl", e);
        }
    }
}
