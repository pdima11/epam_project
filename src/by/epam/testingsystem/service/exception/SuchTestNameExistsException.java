package by.epam.testingsystem.service.exception;

/**
 * Created by User on 23.11.2015.
 */
public class SuchTestNameExistsException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public SuchTestNameExistsException(String message) {
        super(message);
    }

    public SuchTestNameExistsException(String message, Exception e) {
        super(message, e);
    }
}
