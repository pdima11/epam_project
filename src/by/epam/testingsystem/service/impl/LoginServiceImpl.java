package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.service.LoginService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.exception.SuchUserNotExistException;

/**
 * Created by User on 04.11.2015.
 */
public class LoginServiceImpl implements LoginService {

    private LoginServiceImpl(){
    }

    private static class SingletonHolder {
        private static final LoginServiceImpl INSTANCE = new LoginServiceImpl();
    }

    public static LoginServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public User logIn(User user) throws ServiceException{
        String login = user.getLogin();
        String password = user.getPassword();

        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        UserDAO userDAO = daoFactory.getUserDao();

        user = userDAO.getUser(login, password);

        if (user == null) {
            throw new SuchUserNotExistException("User with " + login + "login doesn't exist.");
        } else {
            return user;
        }
    }
}
