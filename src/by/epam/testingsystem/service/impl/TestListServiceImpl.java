package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.service.TestListService;
import by.epam.testingsystem.service.exception.ServiceException;

import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class TestListServiceImpl implements TestListService {
    private static final int TESTS_NUMBER_PER_PAGE = 5;

    private TestListServiceImpl() {
    }

    private static class SingletonHolder {
        private static final TestListServiceImpl INSTANCE = new TestListServiceImpl();
    }

    public static TestListServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public List<Test> getTestList(int page) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        TestDAO testDAO = daoFactory.getTestDao();

        int testsNumberPerPage = TESTS_NUMBER_PER_PAGE;
        int offset = (page - 1) * testsNumberPerPage;

        List<Test> tests = testDAO.getTestList(offset, testsNumberPerPage);

        return tests;
    }

    @Override
    public int getPagesNumber() throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        TestDAO mySQLTestDAO = daoFactory.getTestDao();

        int testsNumberPerPage = TESTS_NUMBER_PER_PAGE;

        int testsNumber = mySQLTestDAO.getTestsNumber();
        int pagesNumber = (int) (Math.ceil((double) testsNumber / testsNumberPerPage));
        return pagesNumber;
    }
}
