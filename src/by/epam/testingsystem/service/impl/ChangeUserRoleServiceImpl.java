package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.service.ChangeUserRoleService;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 23.11.2015.
 */
public class ChangeUserRoleServiceImpl implements ChangeUserRoleService {

    private ChangeUserRoleServiceImpl(){
    }

    private static class SingletonHolder {
        private static final ChangeUserRoleServiceImpl INSTANCE = new ChangeUserRoleServiceImpl();
    }

    public static ChangeUserRoleService getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public boolean changeRole(int userId, String role) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        UserDAO userDAO = daoFactory.getUserDao();

        boolean userUpdated = userDAO.changeUserRole(userId, role);
        return userUpdated;
    }
}
