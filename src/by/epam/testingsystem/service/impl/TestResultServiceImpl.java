package by.epam.testingsystem.service.impl;


import by.epam.testingsystem.domain.Answer;
import by.epam.testingsystem.domain.Question;
import by.epam.testingsystem.domain.Test;

import by.epam.testingsystem.service.StartTestService;
import by.epam.testingsystem.service.TestResultService;
import by.epam.testingsystem.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 13.11.2015.
 */
public class TestResultServiceImpl implements TestResultService {

    private TestResultServiceImpl() {
    }

    private static class SingletonHolder {
        private static final TestResultServiceImpl INSTANCE = new TestResultServiceImpl();
    }

    public static TestResultServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public int getCorrectUserAnswersNumber(int testId, String[] userAnswers) throws ServiceException {
        StartTestService startTestService = QuestionListServiceImpl.getInstance();
        Test test = startTestService.getTest(testId);

        int correctAnswersNumber = testCheck(test.getQuestions(), userAnswers);
        return correctAnswersNumber;
    }

    @Override
    public int getPercent(int correctAnswersNumber, int questionsNumber) throws ServiceException {
        return (int)(((double)correctAnswersNumber / questionsNumber) * 100) ;
    }

    @Override
    public int getAnsweredQuestionsNumber(String[] userAnswers) throws ServiceException {
        int answeredQuestionsNumber = 0;
        for (int i = 0; i < userAnswers.length; i++) {
            if (!userAnswers[i].isEmpty()) {
                answeredQuestionsNumber++;
            }
        }
        return answeredQuestionsNumber;
    }

    private int testCheck(List<Question> questions, String[] userAnswers) {
        int correctAnswersNumber = 0;

        List<Integer> correctAnswersId;
        List<Integer> userAnswersId;
        for (int i = 0; i < questions.size(); i++){
            correctAnswersId = getCorrectAnswersId(questions.get(i));
            userAnswersId = getUserAnswersId(userAnswers[i]);

            boolean isCorrect = checkAnswers(correctAnswersId, userAnswersId);

            if (isCorrect) {
                correctAnswersNumber++;
            }
        }

        return correctAnswersNumber;
    }

    private List<Integer> getCorrectAnswersId(Question question) {
        List<Integer> correctAnswersId = new ArrayList<>();
        for (Answer answer : question.getAnswers()) {
            if (answer.isCorrect()) {
                correctAnswersId.add(answer.getId());
            }
        }
        return correctAnswersId;
    }

    private List<Integer> getUserAnswersId(String userAnswers) {
        String userAnswerPatter = "_";
        String[] stringUserAnswers = userAnswers.split(userAnswerPatter);
        List<Integer> userAnswersId = new ArrayList<>();
        for(int i = 0; i < stringUserAnswers.length; i++) {
            if (!stringUserAnswers[i].isEmpty()) {
                int userAnswerId = Integer.valueOf(stringUserAnswers[i]);
                userAnswersId.add(userAnswerId);
            }
        }
        return userAnswersId;
    }

    private boolean checkAnswers(List<Integer> correctAnswersId, List<Integer> userAnswersId) {
        if (correctAnswersId.size() != userAnswersId.size()) {
            return false;
        }

        for (int i = 0; i < correctAnswersId.size(); i++) {
            int correctAnswerId = correctAnswersId.get(i);
            int userAnswerId = userAnswersId.get(i);

            if (correctAnswerId != userAnswerId) {
                return false;
            }
        }

        return true;
    }

}
