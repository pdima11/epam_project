package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.service.UserListService;
import by.epam.testingsystem.service.exception.ServiceException;

import java.util.List;

/**
 * Created by User on 10.11.2015.
 */
public class UserListServiceImpl implements UserListService {
    private static final int USERS_NUMBER_PER_PAGE = 5;

    private UserListServiceImpl() {
    }

    private static class SingletonHolder {
        private static final UserListServiceImpl INSTANCE = new UserListServiceImpl();
    }

    public static UserListServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public List<User> getUserList(int page) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        UserDAO userDAO = daoFactory.getUserDao();

        int usersNumberPerPage = USERS_NUMBER_PER_PAGE;
        int offset = (page - 1) * usersNumberPerPage;

        List<User> users = userDAO.getUserList(offset, usersNumberPerPage);

        return users;

    }

    @Override
    public int getPagesNumber() throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        UserDAO mySQLUserDAO = daoFactory.getUserDao();

        int usersNumberPerPage = USERS_NUMBER_PER_PAGE;
        int usersNumber = mySQLUserDAO.getUsersNumber();
        int pagesNumber = (int) (Math.ceil((double) usersNumber / usersNumberPerPage));
        return pagesNumber;

    }
}
