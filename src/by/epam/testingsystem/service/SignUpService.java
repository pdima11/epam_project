package by.epam.testingsystem.service;

import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 05.11.2015.
 */
public interface SignUpService{
    boolean signUp(User user) throws ServiceException;
}
