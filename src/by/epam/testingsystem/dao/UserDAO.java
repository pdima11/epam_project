package by.epam.testingsystem.dao;

import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.User;

import java.util.List;

/**
 * Created by User on 14.11.2015.
 */
public interface UserDAO {
    boolean createUser(User user) throws DaoException;
    boolean userExists(String login) throws DaoException;
    User getUser(String login, String password) throws DaoException;
    List<User> getUserList(int offset, int usersNumberPerPage) throws DaoException;
    int getUsersNumber() throws DaoException;
    boolean deleteUser(int userId) throws DaoException;
    boolean changeUserRole(int userId, String role) throws DaoException;
}
