package by.epam.testingsystem.dao;

import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.Test;

import java.util.List;

/**
 * Created by User on 14.11.2015.
 */
public interface TestDAO {
    Test getTest(int testId) throws DaoException;
    List<Test> getTestList(int offset, int testsNumberPerPage) throws DaoException;
    int getTestsNumber() throws DaoException;
    boolean addTest(String testName, int categoryId) throws DaoException;
    boolean updateTest(String testName, int categoryId, int groupUpdateId) throws DaoException;
    int getTestId(String testName) throws DaoException;
    boolean deleteTest(int testId) throws DaoException;
    boolean testNameExists(String testName) throws DaoException;
}
