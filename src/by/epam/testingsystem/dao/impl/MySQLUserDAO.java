package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 04.11.2015.
 */
public class MySQLUserDAO implements UserDAO{
    private static final String MYSQL_CREATE_NEW_USER = "mysql.create.new.user";
    private static final String MYSQL_USER_EXISTS = "mysql.user.exists";
    private static final String MYSQL_GET_USER_BY_LOGIN_PASSWORD = "mysql.get.user.by.login.password";
    private static final String MYSQL_GET_USER_LIST = "mysql.get.user.list";
    private static final String MYSQL_GET_USERS_NUMBER = "mysql.get.users.number";
    private static final String MYSQL_DELETE_USER = "mysql.delete.user";
    private static final String MYSQL_CHANGE_USER_ROLE = "mysql.change.user.role";
    private static final String USER_ID_ATTRIBUTE = "user_id";
    private static final String LOGIN_ATTRIBUTE = "login";
    private static final String PASSWORD_ATTRIBUTE = "password";
    private static final String EMAIL_ATTRIBUTE = "email";
    private static final String ROLE_ATTRIBUTE = "role";
    private static final String USERS_NUMBER_ATTRIBUTE = "users_number";

    private MySQLUserDAO(){}

    private static class SingletonHolder {
        private static final MySQLUserDAO INSTANCE = new MySQLUserDAO();
    }

    public static MySQLUserDAO getInstance() {
        return SingletonHolder.INSTANCE;
    }


    /**
     * This method adds new user to database.
     *
     * @param user new user
     * @return true if user has been successfully added to database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean createUser(User user) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_CREATE_NEW_USER);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ) {

            statement.setString(1, user.getLogin());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());

            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }


    /**
     * This method finds user in database by login and password.
     *
     * @param login user's login
     * @param password user's password
     * @return user if user with this name and password exists, otherwise null
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public User getUser(String login, String password) throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_GET_USER_BY_LOGIN_PASSWORD);
        try(Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery)
           ) {
            statement.setString(1, login);
            statement.setString(2, password);

            ResultSet resultSet = statement.executeQuery();

            User user = null;
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt(USER_ID_ATTRIBUTE));
                user.setLogin(login);
                user.setPassword(password);
                user.setEmail(resultSet.getString(EMAIL_ATTRIBUTE));
                user.setRole(resultSet.getString(ROLE_ATTRIBUTE));
            }

            return user;
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao ", e);
        }
    }

    /**
     * This method gets users from database
     * starting at position <code>offset</code>
     * in number of <code>userNumberPerPage</code>.
     *
     * @param offset start position in database
     * @param usersNumberPerPage number of users
     * @return list of users
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public List<User> getUserList(int offset, int usersNumberPerPage) throws DaoException{
        List<User> users = new ArrayList<>();
        String sqlQuery = Resource.valueOf(MYSQL_GET_USER_LIST);
        try ( Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
              PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ) {

            statement.setInt(1, offset);
            statement.setInt(2, usersNumberPerPage);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt(USER_ID_ATTRIBUTE));
                user.setLogin(resultSet.getString(LOGIN_ATTRIBUTE));
                user.setPassword(resultSet.getString(PASSWORD_ATTRIBUTE));
                user.setEmail(resultSet.getString(EMAIL_ATTRIBUTE));
                user.setRole(resultSet.getString(ROLE_ATTRIBUTE));
                users.add(user);
            }

            return users;
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao ", e);
        }
    }

    /**
     * This method returns the number of users that stored in the database.
     *
     * @return number of users
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public int getUsersNumber() throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_GET_USERS_NUMBER);
        int usersNumber = 0;
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                usersNumber = resultSet.getInt(USERS_NUMBER_ATTRIBUTE);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }

        return usersNumber;
    }

    /**
     * This method deletes user from database by id.
     *
     * @param userId a user ID
     * @return true if user has been successfully deleted from database, otherwise false
     * @throws DaoException
     */
    @Override
    public boolean deleteUser(int userId) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_DELETE_USER);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ) {
            statement.setInt(1, userId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }

    /**
     * This method checks whether user with such <code>login</code> exists in the database.
     *
     * @param login user's login
     * @return true if user exists in the database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean userExists(String login) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_USER_EXISTS);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
        return false;
    }

    /**
     * This method changes user's role with <code>userId</code> on <code>role</code>.
     *
     * @param userId user ID
     * @param role new user's role
     * @return true if role has been successfully changed, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean changeUserRole(int userId, String role) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_CHANGE_USER_ROLE);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, role);
            statement.setInt(2, userId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }

}
