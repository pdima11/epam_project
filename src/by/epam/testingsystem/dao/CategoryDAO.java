package by.epam.testingsystem.dao;

import by.epam.testingsystem.dao.exception.DaoException;


/**
 * Created by User on 22.11.2015.
 */
public interface CategoryDAO {
    boolean addCategory(String category) throws DaoException;
    int getCategoryId(String category) throws DaoException;
}
