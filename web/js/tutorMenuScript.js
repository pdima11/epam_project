/**
 * Created by User on 21.11.2015.
 */
var questionNumber = 0;
var answerNumber = 0;

function init(questionsNumber, answersNumber) {
    jQuery(document).ready(function() {
        questionNumber = questionsNumber;
        answerNumber = answersNumber;
    });
}

function addQuestion(buttonAddAnswerName){
    if (questionNumber == 0) {
        $('#remove-question').css("visibility", "visible");
        $('#add-test').css("visibility", "visible");
    }

    var questionId = "question" + questionNumber;
    var panelBodyId = "panel-body" + questionNumber;
    var inputGroupId = "input-group" + questionNumber;
    var addAnswerButtonId = "answers-group" + questionNumber;
    var questionTextId = "question-text" + (questionNumber++);

    $('#questions').append('<div class="panel panel-danger" id="' + questionId + '"></div>');
    $('#' + questionId).append('<div class="panel-body" id="' + panelBodyId + '"></div>');
    $('#' + panelBodyId).append('<div class="input-group" id="' + inputGroupId + '"></div>');
    $('#' + inputGroupId).append('<span class="input-group-addon"><i class="glyphicon glyphicon-question-sign"></i></span>');
    $('#' + inputGroupId).append('<input type="text" name="' + questionTextId + '" id="' + questionTextId + '"  autocomplete="off" placeholder=" Question Text"/>');
    $('#' + questionTextId).css("width", "100%");
    $('#' + inputGroupId).append('<button type="button" onclick="addAnswer(this.id)" class="btn btn-primary" id="' + addAnswerButtonId + '"></button>');
    $('#' + addAnswerButtonId).append('<span class="glyphicon glyphicon-plus-sign"></span> ' + buttonAddAnswerName + ' ');
    $('#' + addAnswerButtonId).css("width", "100%");
}

function removeQuestion(){
    if (questionNumber == 1) {
        $('#remove-question').css("visibility", "hidden");
        $('#add-test').css("visibility", "hidden");
    }

    if (questionNumber >= 1){
        var questionId = "question" + (--questionNumber);
    }

    $('#' + questionId).remove();
}

function addAnswer(answerGroupId) {
    if (questionNumber == 0) {
        $('#remove-answer').css("visibility", "visible");
    }

    var answerId = "answer" + answerNumber;
    var answerTextId = "answer-text" + answerNumber;
    var funkyradioId = "funkyradio" + answerGroupId + answerNumber;
    var funkyradioPrimaryId = "funkyradio-pr" + answerGroupId + answerNumber++;
    var questionId = $('#' + answerGroupId).parent().parent().attr("id");

    $('#' + questionId).append('<div class="funkyradio" id=' + funkyradioId + '></div>');
    $('#' + funkyradioId).append('<div class="funkyradio-success" id="' + funkyradioPrimaryId +'"></div>');
    $('#' + funkyradioPrimaryId).append('<input type="checkbox" name="' + answerGroupId + '" id="' + answerId + '"/>');
    $('#' + funkyradioPrimaryId).append('<label for="' + answerId +'"><input  type="text" autocomplete="off" ' +
        'placeholder=" Answer Text" name="' + answerGroupId + '"' + ' id="' + answerTextId + '"/></label>')
    $('#' + answerTextId).css("width", "90%");
}

function addTest(form) {
    $('#questions-number').attr("value", questionNumber);

    if (validateTestName() && validateQuestions() && validateAnswers()) {
        alert("Test has been loaded successful.");
        form.submit();
    }
}


function validateQuestions() {
    for (var i = 0; i < questionNumber; i++) {
        var questionTextId = "question-text" + i;
        var questionText =  document.getElementById(questionTextId).value;
        if (questionText.length == 0) {
            alert("Please, enter some value in the field for question.");
            return false;
        }
    }
    return true;
}

function validateTestName() {
    var testNameId = "test-name";
    var categoryId = "category";
    var testName = document.getElementById(testNameId).value;
    var category = document.getElementById(categoryId).value;
    if ((category.length == 0) || (testName.length == 0)) {
        alert("Please, enter some values in the fields for test name or category.");
        return false;
    } else {
        return true;
    }

}

function validateAnswers() {
    var questionsWithAnswersNumber = 0;
    for (var i = 0; i < questionNumber; i++) {
        var questionName = "answers-group" + i; // name for question number i
        var element = document.getElementsByName(questionName);
        var answerExist = false;

        if (element.length < 4) {
            alert("Question should have minimum two answers.");
            return false;
        }

        for (var j = 1; j < element.length; j+=2) {
            if ((element[j]).value.length == 0) {
                alert("Please, enter some values in the fields for answers.");
                return false;
            }
            if (element[j-1].checked) {
                answerExist = true;
            }
        }

        if (answerExist) {
            questionsWithAnswersNumber++;
        }
    }

    if (questionsWithAnswersNumber != questionNumber) {
        alert("Please, check correct answers in each question.");
        return false;
    } else {
        return true;
    }
}