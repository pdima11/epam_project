/**
 * Created by User on 09.11.2015.
 */
function validateNonEmpty(inputField) {
    var regex = /.+/;
    if (!regex.test(inputField.value)) {
        return false;
    } else {
        return true;
    }
}

function checkLogIn(form) {
    if (validateNonEmpty(form["login"]) &&
        validateNonEmpty(form["password"])) {
        form.submit();
    } else {
        alert(document.getElementById('incorrectly-form').innerHTML);
    }
}
