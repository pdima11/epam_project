<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.11.2015
  Time: 19:11
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="../../css/signUpStyle.css"/>
  <script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="../../js/validationSignUpScript.js"></script>
</head>
<body>
<div class="container">
  <div id="signupbox" style="margin-top:50px;" class="mainbox col-md-5 col-md-offset-3">
    <div class="panel panel-primary myborder mycolor ">

      <div class="panel-heading myborder mycolor ">
        <div class="panel-title"><tag:resource key="jsp.title.panel.signup"/> </div>
      </div>

      <div style="padding-top:30px" class="panel-body">
        <form action="${pageContext.request.contextPath}/TestingSystem" method="post" class="form-horizontal">

           <div id="login-input" class="input-group margin-bottom-20">
             <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
             <input id="login" type="text" name="login" class="form-control" autocomplete="off" placeholder="<tag:resource key="jsp.title.login"/> "/>
           </div>
           <div style="display: none" id="login-help" class="help margin-bottom-20">
             <tag:resource key="jsp.title.wrong.signup.login"/>
           </div>

          <div id="email-input" class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <input id="email" type="text" name="email" class="form-control" autocomplete="off" placeholder="<tag:resource key="jsp.title.email"/> "/>
          </div>
          <div style="display: none" id="email-help" class="help margin-bottom-20">
            <tag:resource key="jsp.title.wrong.signup.email"/>
          </div>

          <div id="password-input" class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="password" type="password" name="password" class="form-control" placeholder="<tag:resource key="jsp.title.password"/> "/>
          </div>
          <div style="display: none" id="password-help" class="help margin-bottom-20">
            <tag:resource key="jsp.title.wrong.signup.password"/>
          </div>

          <div id="confirm-password-input" class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="confirm-password" type="password" name="confirm-password" class="form-control" placeholder="<tag:resource key="jsp.title.confirm.password"/> "/>
          </div>
          <div style="display: none" id="confirm-password-help" class="help margin-bottom-20">
            <tag:resource key="jsp.title.wrong.signup.confirm.password"/>
          </div>
          <span id="incorrectly-form" style="display: none"><tag:resource key="jsp.title.wrong.signup"/></span>

          <div style="margin-bottom: 20px;">
            <c:if test="${wrongSignUp == true}">
              <span id="wrong-signup" style="color: #a94442;"><tag:resource key="jsp.title.wrong.signup"/></span>
            </c:if>
          </div>

          <div style="margin-bottom: 20px;">
            <c:if test="${suchLoginExists == true}">
              <span id="wrong-signup" style="color: #a94442;"><tag:resource key="jsp.title.such.login.exists"/></span>
            </c:if>
          </div>

          <div class="row">
            <div class="col-md-12">
              <input type="hidden" name="command" value="Register"/>
              <input type="button" class="btn-u" value="<tag:resource key="jsp.button.signup"/>" onclick="checkSignUp(this.form);"/>
              <a class="btn-u" href="${pageContext.request.contextPath}/jsp/index.jsp">
                <span class="glyphicon glyphicon-home"></span> <tag:resource key="jsp.button.home"/>
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</body>
</html>


