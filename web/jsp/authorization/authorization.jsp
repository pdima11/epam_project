<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.11.2015
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="resource" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/signUpStyle.css"/>
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/validationLogInScript.js"></script>
</head>
<body>

<div class="container">
  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-4 col-md-offset-4">
    <div class="panel panel-primary myborder mycolor" >

      <div class="panel-heading myborder mycolor">
        <div class="panel-title"><tag:resource key="jsp.title.panel.login"/></div>
      </div>

      <div style="padding-top:30px" class="panel-body" >
        <form action="${pageContext.request.contextPath}/TestingSystem" method="POST" class="form-horizontal">
          <div id="login-input" class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="login" type="text" class="form-control" name="login" autocomplete="off" placeholder="<tag:resource key="jsp.title.login"/>">
          </div>


          <div id="password-input" class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="password" type="password" class="form-control" name="password" autocomplete="off" placeholder="<tag:resource key="jsp.title.password"/>">
          </div>

          <span id="incorrectly-form" style="display: none"><tag:resource key="jsp.title.wrong.login"/></span>

          <div class="margin-bottom-20">
            <c:if test="${wrongLogIn == true}">
              <span style="color: #a94442;"><tag:resource key="jsp.title.wrong.login"/></span>
            </c:if>
          </div>

          <input type="hidden" name="command" value="logIn"/>
          <input type="button" class="btn-u" value="<tag:resource key="jsp.button.login"/>" onclick="checkLogIn(this.form)"/>
          <a class="btn-u" href="${pageContext.request.contextPath}/jsp/index.jsp"><tag:resource key="jsp.button.home" /></a>
        </form>

        <div style="border-top: 1px solid#888; padding-top: 15px; font-size: 85%; color: black" >
          <tag:resource key="jsp.message.signup"/>
          <span style="color: #31708f;" class="glyphicon glyphicon-user"></span>
          <a href="${pageContext.request.contextPath}/jsp/authorization/registration.jsp"> <tag:resource key="jsp.link.signup"/></a>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
