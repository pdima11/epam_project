<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ taglib prefix="resource" uri="/WEB-INF/taglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

  <div id="lang-selection" class="btn-group language_selector">
    <button type="button" class="btn btn-default dropdown-toggle mouse-leave" data-toggle="dropdown">
      <img src="<tag:resource key="jsp.select.language" />">
    </button>
    <ul class="dropdown-menu">
      <li style="text-align: center; cursor: pointer">
          <img src="<tag:resource key="jsp.select.language" />">
      </li>
      <c:if test="${language == 'en'}">
        <li style="text-align: center; cursor: pointer">
          <a href="${pageContext.request.contextPath}/TestingSystem?language=ru&command=changeLanguage">
            <img src="<tag:resource key="jsp.not.select.language" />">
          </a>
        </li>
      </c:if>
      <c:if test="${language == 'ru'}">
        <li style="text-align: center; cursor: pointer">
          <a href="${pageContext.request.contextPath}/TestingSystem?language=en&command=changeLanguage">
            <img src="<tag:resource key="jsp.not.select.language" />">
          </a>
        </li>
      </c:if>
    </ul>
  </div>
