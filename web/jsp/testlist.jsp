<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 12.11.2015
  Time: 12:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="../css/bootsnipp.min.css"/>
</head>
<body>
<div>
  <jsp:include page="template/headerTemplate.jsp"/>
</div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <br/>
        <h4><tag:resource key="jsp.title.tests"/></h4>

        <c:if test="${testRemoved == true}">
          <span style="color: #1da934;"><tag:resource key="jsp.message.test.removed"/></span>
        </c:if>
        <c:if test="${testRemoved == false}">
          <span style="color: #a94442;"><tag:resource key="jsp.message.test.not.removed"/></span>
        </c:if>

        <div class="table-responsive">
          <table id="mytable" class="table table-bordred table-striped">
            <thead align="center">
              <th><tag:resource key="jsp.title.test.name"/></th>
              <th><tag:resource key="jsp.title.test.category"/></th>
              <th><tag:resource key="jsp.title.test.questions.number"/></th>
              <c:if test="${user != null}">
                <th><tag:resource key="jsp.title.test.start"/></th>
              </c:if>
              <c:if test="${user.role == 'Admin'}">
                <th><tag:resource key="jsp.title.test.delete"/></th>
              </c:if>
              <c:if test="${user.role == 'Tutor'}">
                <th><tag:resource key="jsp.title.test.modification"/></th>
              </c:if>
            </thead>

            <tbody>
            <c:forEach var="test" items="${tests}">
              <tr>
                <td>${test.name}</td>
                <td>${test.category}</td>
                <td>${test.questionsNumber}</td>

                <c:if test="${user != null}">
                  <td>
                    <form action="${pageContext.request.contextPath}/TestingSystem?test-id=${test.id}" method="post">
                      <button class="btn btn-primary">
                        <span class="glyphicon glyphicon-chevron-right"></span> <tag:resource key="jsp.title.test.start"/>
                      </button>
                      <input type="hidden" name="command" value="startTest"/>
                    </form>
                  </td>
                </c:if>

                <c:if test="${user.role == 'Admin'}">
                <td>
                  <form  action="${pageContext.request.contextPath}/TestingSystem" method="post">
                    <button type="submit" class="btn btn-danger" data-title="Delete" data-toggle="modal" data-target="#delete">
                      <span class="glyphicon glyphicon-trash"></span>
                    </button>
                    <input name="test-id" type="hidden" value="${test.id}"/>
                    <input type="hidden" name="command" value="deleteTest"/>
                  </form>
                </td>
                </c:if>

                <c:if test="${user.role == 'Tutor'}">
                <td>
                  <form  action="${pageContext.request.contextPath}/TestingSystem?test-id=${test.id}" method="post">
                    <button type="submit" class="btn btn-primary">
                      <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    <input type="hidden" name="command" value="modificationTest"/>
                  </form>
                </td>
                </c:if>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-md-12 col-md-offset-2">
        <ul class="pagination">
          <c:if test="${currentPage > 1}">
            <li>
              <a href="${pageContext.request.contextPath}/TestingSystem?command=loadTestList&page=${currentPage - 1}">
                <span style="font-size: 20px" class="glyphicon glyphicon-chevron-left"></span>
              </a>
            </li>
          </c:if>

          <c:forEach begin="1" end="${pagesNumber}" var="i">
            <c:choose>
              <c:when test="${currentPage == i}">
                <li class="active">
                  <span>${i}</span>
                </li>
              </c:when>
              <c:otherwise>
                <li>
                  <a href="${pageContext.request.contextPath}/TestingSystem?command=loadTestList&page=${i}">${i}</a>
                </li>
              </c:otherwise>
            </c:choose>
          </c:forEach>

          <c:if test="${currentPage != pagesNumber}">
            <li>
              <a href="${pageContext.request.contextPath}/TestingSystem?command=loadTestList&page=${currentPage + 1}">
                <span style="font-size: 20px" class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </li>
          </c:if>
        </ul>
      </div>

    </div>
  </div>

</body>
</html>

