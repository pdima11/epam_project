<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 03.11.2015
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title><tag:resource key="jsp.title.menu.home"/></title>
  </head>
  <body>
      <jsp:include page="template/headerTemplate.jsp"/>
      <div class="container">
        <div class="row">
          <div style="margin-top: 10px" class="col-md-4">
            <img src="${pageContext.request.contextPath}/images/home.png"/>
          </div>
          <div style="margin-top: 40px; font-family: cursive; font-size: 15px" class="col-md-4 col-md-offset-2">
            <h2 style="font-family: 'Times New Roman'"><tag:resource key="jsp.title.project"/></h2>
            <tag:resource key="jsp.title.home"/>
          </div>
        </div>
      </div>
  </body>
</html>
