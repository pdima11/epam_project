<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10.11.2015
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootsnipp.min.css"/>
</head>
<body>
  <div>
    <jsp:include page="../../../jsp/template/headerTemplate.jsp"/>
  </div>

  <c:if test="${user.role == 'Admin'}">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <br/>
          <h4><tag:resource key="jsp.title.users"/></h4>

          <c:if test="${roleChanged == true}">
            <span style="color: #1da934;"><tag:resource key="jsp.message.role.changed"/></span>
          </c:if>
          <c:if test="${roleChanged == false}">
            <span style="color: #a94442;"><tag:resource key="jsp.message.role.not.changed"/></span>
          </c:if>
          <c:if test="${userRemoved == true}">
            <span style="color: #1da934;"><tag:resource key="jsp.message.user.removed"/></span>
          </c:if>
          <c:if test="${userRemoved == false}">
            <span style="color: #a94442;"><tag:resource key="jsp.message.user.removed"/></span>
          </c:if>

          <div class="table-responsive">

            <table id="mytable" class="table table-bordred table-striped">

              <thead align="center">
                <th>Id</th>
                <th><tag:resource key="jsp.title.login"/></th>
                <th><tag:resource key="jsp.title.email"/></th>
                <th><tag:resource key="jsp.title.role"/></th>
                <th><tag:resource key="jsp.title.change.role"/></th>
                <th><tag:resource key="jsp.title.delete"/></th>
              </thead>

              <tbody>
                <c:forEach var="user" items="${users}">

                  <tr>
                    <td>${user.id}</td>
                    <td>${user.login}</td>
                    <td>${user.email}</td>
                    <td>${user.role}</td>

                    <td>
                      <form action="${pageContext.request.contextPath}/TestingSystem" method="post">
                        <div>
                          <c:if test="${user.role == 'Tutor'}">
                            <input type="radio" name="role" id="role-user" value="User" />
                            <label for="role-user">User</label>
                            <input type="radio" name="role" id="role-admin" value="Admin" />
                            <label for="role-admin">Admin</label>
                          </c:if>
                          <c:if test="${user.role == 'Admin'}">
                            <input type="radio" name="role" id="role-user" value="User" />
                            <label for="role-user">User</label>
                            <input type="radio" name="role" id="role-tutor" value="Tutor" />
                            <label for="role-tutor">Tutor</label>
                          </c:if>
                          <c:if test="${user.role == 'User'}">
                            <input type="radio" name="role" id="role-tutor" value="Tutor" />
                            <label for="role-tutor">Tutor</label>
                            <input type="radio" name="role" id="role-admin" value="Admin" />
                            <label for="role-admin">Admin</label>
                          </c:if>
                        </div>
                        <input type="hidden" name="user-id" value="${user.id}"/>
                        <input type="hidden" name="command" value="changeUserRole"/>
                        <button type="submit" class="btn btn-primary ">
                          <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                      </form>
                    </td>

                    <td>
                     <form  action="${pageContext.request.contextPath}/TestingSystem" method="post">
                       <input name="user-id" type="hidden" value="${user.id}"/>
                       <input type="hidden" name="command" value="deleteUser"/>
                       <button type="submit" class="btn btn-danger">
                         <span class="glyphicon glyphicon-trash"></span>
                       </button>
                     </form>
                    </td>
                  </tr>

                </c:forEach>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12 col-md-offset-2">
        <ul class="pagination">
          <c:if test="${currentPage > 1}">
            <li>
              <a href="${pageContext.request.contextPath}/TestingSystem?command=loadUserList&page=${currentPage - 1}">
                <span style="font-size: 20px" class="glyphicon glyphicon-chevron-left"></span>
              </a>
            </li>
          </c:if>

          <c:forEach begin="1" end="${pagesNumber}" var="i">
            <c:choose>
              <c:when test="${currentPage == i}">
                <li class="active">
                  <span>${i}</span>
                </li>
              </c:when>
              <c:otherwise>
                <li>
                  <a href="${pageContext.request.contextPath}/TestingSystem?command=loadUserList&page=${i}">${i}</a>
                </li>
              </c:otherwise>
            </c:choose>
          </c:forEach>

          <c:if test="${currentPage != pagesNumber}">
            <li>
              <a href=${pageContext.request.contextPath}/TestingSystem?command=loadUserList&page=${currentPage + 1}>
                <span style="font-size: 20px" class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </li>
          </c:if>
        </ul>
      </div>
    </div>
  </div>
  </c:if>

</body>
</html>
