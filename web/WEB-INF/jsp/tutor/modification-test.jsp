<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 28.11.2015
  Time: 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="questionNumber" value="0"/>
<c:set var="answerNumber" value="0"/>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/starttest.css"/>
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/tutorMenuScript.js"></script>
</head>
<body>
  <jsp:include page="../../../jsp/template/headerTemplate.jsp"/>

  <c:if test="${user.role == 'Tutor'}">
    <div style="margin-top: 50px;" class="container">
      <div class="row">
        <form action="${pageContext.request.contextPath}/TestingSystem" method="post">

          <div class="col-md-4 col-md-offset-2">
            <div id="testname-input" class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
              <input type="text" name="test-name" id="test-name" class="form-control" value="${test.name}" autocomplete="off"/>
            </div>

            <div id="category-input" class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
              <input type="text" name="category" id="category" class="form-control" value="${test.category}" autocomplete="off"/>
            </div>
          </div>

          <div id="questions" class="col-md-8 col-md-offset-2" style="margin-top: 40px">
            <c:forEach var="question" items="${test.questions}">
              <div class="panel panel-danger" id="question${questionNumber}">
                <div class="panel-body" id="panel-body${questionNumber}">

                  <div class="input-group" id="input-group${questionNumber}">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-question-sign"></i></span>
                    <input type="text" name="question-text${questionNumber}" id="question-text${questionNumber}" value="${question.question}" autocomplete="off" style="width: 100%;">
                    <button type="button" onclick="addAnswer(this.id)" class="btn btn-primary" id="answers-group${questionNumber}" style="width: 100%;">
                      <span class="glyphicon glyphicon-plus-sign"></span> <tag:resource key="jsp.button.add.answer"/>
                    </button>
                  </div>

                  <c:forEach var="answer" items="${question.answers}">
                    <div class="funkyradio" id="funkyradioanswers-group${questionNumber}${answerNumber}">
                      <div class="funkyradio-success" id="funkyradio-pranswers-group${questionNumber}${answerNumber}">
                        <c:if test="${answer.correct}">
                          <input type="checkbox" checked="checked" value="on" name="answers-group${questionNumber}" id="answer${answerNumber}">
                        </c:if>
                        <c:if test="${!answer.correct}">
                          <input type="checkbox" name="answers-group${questionNumber}" id="answer${answerNumber}">
                        </c:if>

                        <label for="answer${answerNumber}">
                          <input type="text" value="${answer.answer}" autocomplete="off" placeholder=" Answer Text" name="answers-group${questionNumber}" id="answer-text${answerNumber}" style="width: 90%;">
                        </label>
                      </div>
                    </div>
                    <c:set var="answerNumber" value="${answerNumber+1}"/>
                  </c:forEach>

                </div>
              </div>
              <c:set var="questionNumber" value="${questionNumber+1}"/>
            </c:forEach>

          </div>

          <div class="col-md-2 col-md-offset-2" style="margin-bottom: 20px" >
            <button id="add-question" type="button" onclick="addQuestion('<tag:resource key="jsp.button.add.answer"/>')" class="btn btn-primary">
              <span class="glyphicon glyphicon-plus"></span> <tag:resource key="jsp.button.add.question"/>
            </button>
          </div>

          <div class="col-md-2" style="margin-bottom: 20px">
            <button id="remove-question" type="button" onclick="removeQuestion()"
                    class="btn btn-danger">
              <span class="glyphicon glyphicon-remove"></span> <tag:resource key="jsp.button.remove.question"/>
            </button>
          </div>

          <input type="hidden" name="questions-number" id="questions-number" value="0"/>
          <input type="hidden" name="command" value="addTest"/>
          <input type="hidden" name="group-update-id" value="${test.groupUpdateId}"/>
          <input id="add-test" type="button" onclick="addTest(this.form)"
                 class="btn btn-primary" value="<tag:resource key="jsp.button.update.test"/>"/>
        </form>
      </div>
    </div>
  </c:if>
</body>
</html>
<script type="text/javascript">
  window.onload = init(${questionNumber}, ${answerNumber});
</script>
