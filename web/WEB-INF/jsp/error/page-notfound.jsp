<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 30.11.2015
  Time: 22:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/error-page.css"/>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="error-template">
        <h1>Oops!</h1>
        <h2>404 Page Not Found</h2>
        <div class="error-details">
          Requested page not found!
        </div>
        <div class="error-actions">
          <a href="${pageContext.request.contextPath}/jsp/index.jsp" class="btn btn-primary btn-lg">
            <span class="glyphicon glyphicon-home"></span> <tag:resource key="jsp.button.home"/>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
