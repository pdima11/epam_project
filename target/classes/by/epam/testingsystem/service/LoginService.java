package by.epam.testingsystem.service;

import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 04.11.2015.
 */
public interface LoginService {
    User logIn(User user) throws ServiceException;
}
