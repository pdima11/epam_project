package by.epam.testingsystem.service;

import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 23.11.2015.
 */
public interface DeleteTestService {
    boolean deleteTest(int testId) throws ServiceException;
}
