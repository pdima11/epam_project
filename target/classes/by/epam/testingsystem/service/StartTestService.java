package by.epam.testingsystem.service;

import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.service.exception.ServiceException;

import java.rmi.server.ServerCloneException;

/**
 * Created by User on 15.11.2015.
 */
public interface StartTestService {
    Test getTest(int testId) throws ServiceException;
}
