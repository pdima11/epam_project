package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.AnswerDAO;
import by.epam.testingsystem.dao.CategoryDAO;
import by.epam.testingsystem.dao.QuestionDAO;
import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.dao.impl.MySQLAnswerDAO;
import by.epam.testingsystem.dao.impl.MySQLCategoryDAO;
import by.epam.testingsystem.dao.impl.MySQLQuestionDAO;
import by.epam.testingsystem.dao.impl.MySQLTestDAO;
import by.epam.testingsystem.domain.Answer;
import by.epam.testingsystem.domain.Question;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.service.AddTestService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.exception.SuchTestNameExistsException;

import java.util.List;

/**
 * Created by User on 22.11.2015.
 */
public class AddTestServiceImpl implements AddTestService{
    private static final int CATEGORY_NOT_EXIST = -1;
    private static final int ADD_TEST = 0;

    private AddTestServiceImpl(){
    }

    private static class SingletonHolder {
        private static final AddTestServiceImpl INSTANCE = new AddTestServiceImpl();
    }

    public static AddTestServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public boolean addTest(Test test) throws ServiceException{
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        TestDAO testDAO = daoFactory.getTestDao();
        CategoryDAO categoryDAO = daoFactory.getCategoryDao();

        boolean testAdded = false;
        String testName = test.getName();
        String testCategory = test.getCategory();
        int groupUpdateId = test.getGroupUpdateId();

        boolean testNameExist = testDAO.testNameExists(testName);

        if (testNameExist && (groupUpdateId == ADD_TEST)) {
            throw new SuchTestNameExistsException("Test with" + testName + "name already exists.");
        }

        boolean categoryAdded = true;
        int categoryId = categoryDAO.getCategoryId(testCategory);

        if (categoryId == CATEGORY_NOT_EXIST) {
            categoryAdded = categoryDAO.addCategory(testCategory);
        }

        if (categoryAdded) {
            categoryId = categoryDAO.getCategoryId(testCategory);

            if (groupUpdateId == ADD_TEST) {
                testAdded = testDAO.addTest(testName, categoryId);
            } else {
                testAdded = testDAO.updateTest(testName, categoryId, groupUpdateId);
            }

            if (testAdded) {
                int testId = testDAO.getTestId(testName);
                return addQuestions(daoFactory, testId, test.getQuestions());
            } else {
                return false;
            }
        }

        return testAdded;
    }

    private boolean addQuestions(DAOFactory daoFactory, int testId, List<Question> questions) throws DaoException{
        QuestionDAO questionDAO = daoFactory.getQuestionDao();

        for (Question question : questions) {
            String questionText = question.getQuestion();
            boolean oneCorrectAnswer = question.isOneCorrectAnswer();
            boolean questionAdded = questionDAO.addQuestion(testId, questionText, oneCorrectAnswer);

            if (questionAdded) {
                int questionId = questionDAO.getQuestionId(testId, questionText);
                boolean answerAdded = addAnswers(daoFactory, questionId, question.getAnswers());
                if (!answerAdded) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private boolean addAnswers(DAOFactory daoFactory, int questionId, List<Answer> answers) throws DaoException {
        AnswerDAO answerDAO = daoFactory.getAnswerDao();

        for (Answer answer : answers) {
            String answerText = answer.getAnswer();
            boolean correct = answer.isCorrect();
            boolean answerAdded = answerDAO.addAnswer(questionId, answerText, correct);

            if (!answerAdded) {
                return false;
            }
        }
        return true;
    }
}
