package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.service.SignUpService;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 15.11.2015.
 */
public class SignUpServiceImpl implements SignUpService{

    private SignUpServiceImpl() {
    }

    private static class SingletonHolder {
        private static final SignUpServiceImpl INSTANCE = new SignUpServiceImpl();
    }

    public static SignUpServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public boolean signUp(User user) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        UserDAO userDAO = daoFactory.getUserDao();

        boolean suchLoginExists = userDAO.userExists(user.getLogin());
        if (suchLoginExists) {
            return false;
        }

        boolean userCreated = userDAO.createUser(user);
        return userCreated;
    }
}
