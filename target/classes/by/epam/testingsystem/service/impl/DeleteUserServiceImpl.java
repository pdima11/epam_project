package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.service.DeleteUserService;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 15.11.2015.
 */
public class DeleteUserServiceImpl implements DeleteUserService{

    private DeleteUserServiceImpl(){
    }

    private static class SingletonHolder {
        private static final DeleteUserServiceImpl INSTANCE = new DeleteUserServiceImpl();
    }

    public static DeleteUserServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public boolean deleteUser(int userId) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        UserDAO userDAO = daoFactory.getUserDao();
        boolean userDeleted = userDAO.deleteUser(userId);
        return userDeleted;
    }
}
