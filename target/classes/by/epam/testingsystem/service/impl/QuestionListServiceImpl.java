package by.epam.testingsystem.service.impl;


import by.epam.testingsystem.dao.AnswerDAO;
import by.epam.testingsystem.dao.QuestionDAO;
import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.domain.Answer;
import by.epam.testingsystem.domain.Question;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.service.StartTestService;
import by.epam.testingsystem.service.exception.ServiceException;

import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class QuestionListServiceImpl implements StartTestService {
    private static final QuestionListServiceImpl instance = new QuestionListServiceImpl();

    private QuestionListServiceImpl() {

    }

    public static QuestionListServiceImpl getInstance() {
        return instance;
    }

    @Override
    public Test getTest(int testId) throws ServiceException{
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null){
            throw new ServiceException("DAOFactory returned null");
        }

        TestDAO testDAO = daoFactory.getTestDao();
        QuestionDAO questionDAO = daoFactory.getQuestionDao();
        AnswerDAO answerDAO = daoFactory.getAnswerDao();

        Test test = testDAO.getTest(testId);

        List<Question> questions = questionDAO.getQuestionList(testId);

        for (Question question : questions) {
            int questionId = question.getId();
            List<Answer> answers = answerDAO.getAnswerList(questionId);
            question.setAnswers(answers);
        }

        test.setQuestions(questions);
        return test;
    }
}
