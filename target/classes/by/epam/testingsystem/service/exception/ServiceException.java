package by.epam.testingsystem.service.exception;

import by.epam.testingsystem.controller.command.exception.CommandException;

/**
 * Created by User on 04.11.2015.
 */
public class ServiceException extends CommandException {
    private static final long serialVersionUID = 1L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Exception e) {
        super(message, e);
    }
}
