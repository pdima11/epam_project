package by.epam.testingsystem.service.exception;

/**
 * Created by User on 23.11.2015.
 */
public class SuchUserNotExistException extends ServiceException{
    private static final long serialVersionUID = 1L;

    public SuchUserNotExistException(String message) {
        super(message);
    }

    public SuchUserNotExistException(String message, Exception e) {
        super(message, e);
    }
}
