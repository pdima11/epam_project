package by.epam.testingsystem.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by User on 24.11.2015.
 */
public class CurrentQueryFilter implements Filter{
    private static final String COMMAND_PARAMETER = "command";
    private static final String CHANGE_LANGUAGE_PARAMETER = "changeLanguage";
    private static final String CURRENT_QUERY_PARAMETER = "currentQuery";

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        StringBuffer query = ((HttpServletRequest)request).getRequestURL();
        String commandName = request.getParameter(COMMAND_PARAMETER);

        if (!CHANGE_LANGUAGE_PARAMETER.equals(commandName)) {
            String stringQuery = ((HttpServletRequest) request).getQueryString();

            if (stringQuery != null) {
                query.append('?');
                query.append(stringQuery);
                if (query.indexOf(commandName) == -1){
                    query.append("&command=");
                    query.append(commandName);
                }
            }

            ((HttpServletRequest) request).getSession(true).setAttribute(CURRENT_QUERY_PARAMETER, query.toString());
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
