package by.epam.testingsystem.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by User on 18.11.2015.
 */
public class LocaleResourceTag extends TagSupport {
    private static final String LANGUAGE_PARAMETER = "language";
    private static final String PROPERTIES_FILE_PATH = "by.epam.testingsystem.resource.resource";

    private String key;

    @Override
    public int doStartTag() throws JspException {
        JspWriter writer = pageContext.getOut();
        Locale locale;
        String language = (String) pageContext.getSession().getAttribute(LANGUAGE_PARAMETER);
        if (language != null) {
            locale = new Locale(language);
        } else {
            locale = pageContext.getRequest().getLocale();
        }

        ResourceBundle resource = ResourceBundle.getBundle(PROPERTIES_FILE_PATH, locale);

        try {
            writer.print(resource.getString(key));
        } catch (IOException e) {
            //logger
            e.printStackTrace();
        }

        return SKIP_BODY;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
