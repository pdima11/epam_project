package by.epam.testingsystem.controller.command;

/**
 * Created by User on 05.11.2015.
 */
public enum CommandType {
    LOG_IN, LOG_OUT, REGISTER, CHANGE_LANGUAGE, LOAD_USER_LIST, DELETE_USER,
    LOAD_TEST_LIST, START_TEST, TEST_RESULT, ADD_TEST, DELETE_TEST, CHANGE_USER_ROLE, MODIFICATION_TEST;

    public static CommandType getCommandName(String element) throws IllegalArgumentException{
        String[] items = element.split("(?<=[a-z])(?=[A-Z])");
        CommandType commandName = CommandType.valueOf(String.join("_", items).toUpperCase());

        return commandName;
    }
}
