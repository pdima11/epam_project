package by.epam.testingsystem.controller.command;

import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.resource.Resource;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 04.11.2015.
 */
public interface Command {
    String execute(HttpServletRequest request) throws CommandException;
}
