package by.epam.testingsystem.controller.command.impl.user;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.SignUpService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.impl.SignUpServiceImpl;
import by.epam.testingsystem.util.SignUpValidator;


import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 05.11.2015.
 */
public class SignUpCommand implements Command {
    private static final String USER_LOGIN_PARAMETER = "login";
    private static final String USER_PASSWORD_PARAMETER = "password";
    private static final String USER_EMAIL_PARAMETER = "email";
    private static final String USER_CONFIRM_PASSWORD_PARAMETER = "confirm-password";
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String FORWARD_PAGE_REGISTRATION = "forward.page.registration";
    private static final String SUCH_LOGIN_EXISTS_PARAMETER = "suchLoginExists";
    private static final String WRONG_SIGN_UP_PARAMETER = "wrongSignUp";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        SignUpService signUpService = SignUpServiceImpl.getInstance();
        String forwardPage;

        String login = request.getParameter(USER_LOGIN_PARAMETER);
        String email = request.getParameter(USER_EMAIL_PARAMETER);
        String password = request.getParameter(USER_PASSWORD_PARAMETER);
        String confirmPassword = request.getParameter(USER_CONFIRM_PASSWORD_PARAMETER);

        User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setPassword(password);

        boolean isSignUpValid = SignUpValidator.isSignUpValid(user, confirmPassword);

        try {
            if (isSignUpValid) {
                boolean signUp = signUpService.signUp(user);
                if (signUp) {
                    forwardPage = Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
                } else {
                    forwardPage = Resource.valueOf(FORWARD_PAGE_REGISTRATION);
                    request.setAttribute(SUCH_LOGIN_EXISTS_PARAMETER, true);
                }
            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_REGISTRATION);
                request.setAttribute(WRONG_SIGN_UP_PARAMETER, true);
            }
            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing LoginServiceImpl", e);
        }
    }
}
