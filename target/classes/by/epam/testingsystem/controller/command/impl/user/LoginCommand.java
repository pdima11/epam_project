package by.epam.testingsystem.controller.command.impl.user;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.LoginService;
import by.epam.testingsystem.service.exception.SuchUserNotExistException;
import by.epam.testingsystem.service.impl.LoginServiceImpl;
import by.epam.testingsystem.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 05.11.2015.
 */
public class LoginCommand implements Command {
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String WRONG_LOGIN = "wrongLogIn";
    private static final String USER_PARAMETER = "user";
    private static final String USER_LOGIN_PARAMETER = "login";
    private static final String USER_PASSWORD_PARAMETER = "password";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        String forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);

        try {
            String login = request.getParameter(USER_LOGIN_PARAMETER);
            String password = request.getParameter(USER_PASSWORD_PARAMETER);

            if ((login != null) && (password != null)) {
                User userInput = new User();
                userInput.setLogin(login);
                userInput.setPassword(password);

                LoginService loginService = LoginServiceImpl.getInstance();
                User user = loginService.logIn(userInput);

                request.getSession(true).setAttribute(USER_PARAMETER, user);
            }

        } catch (SuchUserNotExistException e) {
            forwardPage = Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
            request.setAttribute(WRONG_LOGIN, true);
        } catch (ServiceException e) {
            throw new CommandException("Error while executing LoginServiceImpl", e);
        }

        return forwardPage;
    }
}
