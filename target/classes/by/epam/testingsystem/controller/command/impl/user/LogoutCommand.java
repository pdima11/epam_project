package by.epam.testingsystem.controller.command.impl.user;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.resource.Resource;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 05.11.2015.
 */
public class LogoutCommand implements Command {
    private static final String USER_PARAMETER = "user";
    private static final String FORWARD_PAGE_MAIN = "forward.page.home";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        String forwardPage = Resource.valueOf(FORWARD_PAGE_MAIN);
        request.getSession().removeAttribute(USER_PARAMETER);
        return forwardPage;
    }
}
