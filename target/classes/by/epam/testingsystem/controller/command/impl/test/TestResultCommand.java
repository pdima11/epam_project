package by.epam.testingsystem.controller.command.impl.test;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.TestResultService;
import by.epam.testingsystem.service.impl.TestResultServiceImpl;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 13.11.2015.
 */
public class TestResultCommand implements Command {
    private static final String FORWARD_PAGE_TEST_RESULT = "forward.page.test.result";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String USER_ANSWERS_ID_PARAMETER = "user-answers-id";
    private static final String CORRECT_ANSWERS_NUMBER = "correctAnswersNumber";
    private static final String TEST_QUESTION_NUMBER = "testQuestionsNumber";
    private static final String TEST_ID_PARAMETER = "test-id";
    private static final String QUESTIONS_NUMBER_PARAMETER = "questions-number";
    private static final String PERCENT_PARAMETER = "percent";
    private static final String ATTEMPTED_PARAMETER = "attempted";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = Resource.valueOf(FORWARD_PAGE_TEST_RESULT);

        try {
            String testIdParameter = request.getParameter(TEST_ID_PARAMETER);
            String questionsNumberParameter = request.getParameter(QUESTIONS_NUMBER_PARAMETER);
            boolean isTestIdParameterInteger = RequestParameterValidator.isInteger(testIdParameter);
            boolean isQuestionNumberParameterInteger = RequestParameterValidator.isInteger(testIdParameter);

            if (isTestIdParameterInteger && isQuestionNumberParameterInteger) {

                int testId = Integer.valueOf(testIdParameter);
                int questionsNumber = Integer.valueOf(questionsNumberParameter);

                TestResultService testResultService = TestResultServiceImpl.getInstance();

                String[] userAnswers = getUserAnswers(questionsNumber, request);

                int correctAnswersNumber = testResultService.getCorrectUserAnswersNumber(testId, userAnswers);
                int percent = testResultService.getPercent(correctAnswersNumber, questionsNumber);
                int answeredQuestionsNumber  = testResultService.getAnsweredQuestionsNumber(userAnswers);
                request.setAttribute(PERCENT_PARAMETER, percent);
                request.setAttribute(ATTEMPTED_PARAMETER, answeredQuestionsNumber);
                request.setAttribute(CORRECT_ANSWERS_NUMBER, correctAnswersNumber);
                request.setAttribute(TEST_QUESTION_NUMBER, questionsNumber);
            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);
            }

            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing LoginServiceImpl", e);
        }

    }

    private String[] getUserAnswers(int questionsNumber, HttpServletRequest request) {
        String[] userAnswers = new String[questionsNumber];

        for (int i = 0; i < questionsNumber; i++) {
            userAnswers[i] = request.getParameter(USER_ANSWERS_ID_PARAMETER + String.valueOf(i));
        }

        return userAnswers;
    }
}
