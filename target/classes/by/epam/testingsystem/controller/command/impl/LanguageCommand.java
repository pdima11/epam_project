package by.epam.testingsystem.controller.command.impl;

import by.epam.testingsystem.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 07.11.2015.
 */
public class LanguageCommand implements Command {
    private static final String LANGUAGE_PARAMETER = "language";
    private static final String ENGLISH_LANGUAGE_PARAMETER = "en";
    private static final String RUSSIAN_LANGUAGE_PARAMETER = "ru";
    private static final String CURRENT_QUERY_PARAMETER = "currentQuery";

    @Override
    public String execute(HttpServletRequest request) {
        String forwardPage = (String) request.getSession(true).getAttribute(CURRENT_QUERY_PARAMETER);

        String language = (String)request.getSession().getAttribute(LANGUAGE_PARAMETER);

        if (ENGLISH_LANGUAGE_PARAMETER.equals(language)) {
            language = RUSSIAN_LANGUAGE_PARAMETER;
        } else {
            language = ENGLISH_LANGUAGE_PARAMETER;
        }

        request.getSession().setAttribute(LANGUAGE_PARAMETER, language);

        return forwardPage;
    }
}
