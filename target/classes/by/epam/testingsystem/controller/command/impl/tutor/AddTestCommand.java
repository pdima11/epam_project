package by.epam.testingsystem.controller.command.impl.tutor;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.Answer;
import by.epam.testingsystem.domain.Question;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.AddTestService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.exception.SuchTestNameExistsException;
import by.epam.testingsystem.service.impl.AddTestServiceImpl;
import by.epam.testingsystem.util.RequestParameterValidator;
import by.epam.testingsystem.util.TestValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 20.11.2015.
 */
public class AddTestCommand implements Command {
    private static final String FORWARD_PAGE_TEST_LIST = "forward.page.tutor.menu";
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String QUESTION_PARAMETER = "question-text";
    private static final String QUESTIONS_NUMBER_PARAMETER = "questions-number";
    private static final String TEST_NAME_PARAMETER = "test-name";
    private static final String GROUP_UPDATE_ID_PARAMETER = "group-update-id";
    private static final String TEST_CATEGORY_PARAMETER = "category";
    private static final String ANSWERS_GROUP_PARAMETER = "answers-group";
    private static final String CORRECT_ANSWER = "on";
    private static final String SUCCESSFUL_ADD_TEST = "testAdded";
    private static final String SUCH_TEST_NAME_EXISTS_PARAMETER = "testNameExists";
    private static final String USER_PARAMETER = "user";
    private static final String ROLE_PARAMETER = "Tutor";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        String forwardPage = Resource.valueOf(FORWARD_PAGE_TEST_LIST);

        HttpSession session = request.getSession(false);

        if (session == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        User currentUser = (User) session.getAttribute(USER_PARAMETER);
        if (currentUser == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        if (!ROLE_PARAMETER.equals(currentUser.getRole())) {
            return Resource.valueOf(FORWARD_PAGE_HOME);
        }

        try {
            String questionNumberParameter = request.getParameter(QUESTIONS_NUMBER_PARAMETER);
            String groupUpdateIdParameter = request.getParameter(GROUP_UPDATE_ID_PARAMETER);
            boolean testAdded = false;
            boolean isQuestionNumberParameterInteger = RequestParameterValidator.isInteger(questionNumberParameter);

            if (isQuestionNumberParameterInteger) {
                boolean isGroupUpdateIdParameterInteger = RequestParameterValidator.isInteger(groupUpdateIdParameter);

                int questionsNumber = Integer.valueOf(questionNumberParameter);
                String testName = request.getParameter(TEST_NAME_PARAMETER);
                String category = request.getParameter(TEST_CATEGORY_PARAMETER);
                List<Question> questions = getQuestions(questionsNumber, request);

                Test test = new Test();
                if (isGroupUpdateIdParameterInteger) {
                    int groupUpdateId = Integer.valueOf(groupUpdateIdParameter);
                    test.setGroupUpdateId(groupUpdateId);
                }
                test.setName(testName);
                test.setCategory(category);
                test.setQuestions(questions);

                boolean isTestValid = TestValidator.isTestValid(test);

                if (isTestValid) {
                    AddTestService addTestService = AddTestServiceImpl.getInstance();
                    testAdded = addTestService.addTest(test);
                }

            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);
            }

            if (testAdded){
                request.setAttribute(SUCCESSFUL_ADD_TEST, true);
            } else {
                request.setAttribute(SUCCESSFUL_ADD_TEST, false);
            }

        }catch (SuchTestNameExistsException e) {
            request.setAttribute(SUCH_TEST_NAME_EXISTS_PARAMETER, true);
        } catch (ServiceException e) {
            throw new CommandException("Error while executing AddServiceServiceImpl", e);
        }

        return forwardPage;
    }

    private List<Question> getQuestions(int questionsNumber, HttpServletRequest request) {
        List<Question> questions = new ArrayList<>();

        for (int i = 0; i < questionsNumber; i++) {
            String questionText = request.getParameter(QUESTION_PARAMETER + String.valueOf(i));

            String[] answersGroup = request.getParameterValues(ANSWERS_GROUP_PARAMETER + String.valueOf(i));

            if (answersGroup == null) {
                return null;
            }

            List<Answer> answers = getAnswers(answersGroup);

            int correctAnswersNumber = answersGroup.length - answers.size();

            Question question = new Question();

            if (correctAnswersNumber == 1){
                question.setOneCorrectAnswer(true);
            }

            question.setQuestion(questionText);
            question.setAnswers(answers);

            questions.add(question);
        }

        return questions;
    }

    private List<Answer> getAnswers(String[] answerGroup) {
        List<Answer> answers = new ArrayList<>();
        boolean isCorrectAnswer = false;

        for (int i = 0; i < answerGroup.length; i++) {
            if (CORRECT_ANSWER.equals(answerGroup[i])) {
                isCorrectAnswer = true;
            } else {
                Answer answer = new Answer();
                answer.setAnswer(answerGroup[i]);
                answer.setCorrect(isCorrectAnswer);
                isCorrectAnswer = false;
                answers.add(answer);
            }
        }

        return answers;
    }

}
