package by.epam.testingsystem.controller;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.CommandHelper;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 03.11.2015.
 */
public class TestingSystemController extends HttpServlet {
    private static final CommandHelper helper = new CommandHelper();
    private static final String COMMAND_PARAMETER = "command";
    private static final String CHANGE_LANGUAGE_PARAMETER = "changeLanguage";
    private static final String FORWARD_PAGE_ERROR = "forward.page.error";

    private static final Logger logger = LogManager.getRootLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String commandName = request.getParameter(COMMAND_PARAMETER);
        Command command = helper.getCommand(commandName);

        try {
            String forwardPage = command.execute(request);

            if (CHANGE_LANGUAGE_PARAMETER.equals(commandName)) {
                response.sendRedirect(forwardPage);
            } else {
                request.getRequestDispatcher(forwardPage).forward(request, response);
            }
        } catch (DaoException e) {
            logger.error("", e);
            e.printStackTrace();
            String forwardPage = Resource.valueOf(FORWARD_PAGE_ERROR);
            request.getRequestDispatcher(forwardPage).forward(request, response);
        } catch (ServiceException e) {
            logger.error("", e);
            e.printStackTrace();
            String forwardPage = Resource.valueOf(FORWARD_PAGE_ERROR);
            request.getRequestDispatcher(forwardPage).forward(request, response);
        } catch (CommandException e) {
            logger.error("", e);
            e.printStackTrace();
            String forwardPage = Resource.valueOf(FORWARD_PAGE_ERROR);
            request.getRequestDispatcher(forwardPage).forward(request, response);
        } catch (Exception e) {
            logger.error("", e);
            e.printStackTrace();
            String forwardPage = Resource.valueOf(FORWARD_PAGE_ERROR);
            request.getRequestDispatcher(forwardPage).forward(request, response);
        }
    }

}
