package by.epam.testingsystem.dao.factory;

import by.epam.testingsystem.dao.*;
import by.epam.testingsystem.dao.impl.*;

/**
 * Created by User on 03.11.2015.
 */
public final class MySQLDAOFactory extends DAOFactory{
    private MySQLDAOFactory() {
    }

    private static class SingletonHolder {
        private static final MySQLDAOFactory INSTANCE = new MySQLDAOFactory();
    }

    public static MySQLDAOFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public UserDAO getUserDao() {
        return MySQLUserDAO.getInstance();
    }

    public TestDAO getTestDao() {
        return MySQLTestDAO.getInstance();
    }

    public QuestionDAO getQuestionDao() {
        return MySQLQuestionDAO.getInstance();
    }

    public AnswerDAO getAnswerDao() {
        return MySQLAnswerDAO.getInstance();
    }

    public CategoryDAO getCategoryDao() {
        return MySQLCategoryDAO.getInstance();
    }
}
