package by.epam.testingsystem.dao.factory;

import by.epam.testingsystem.dao.AnswerDAO;
import by.epam.testingsystem.dao.QuestionDAO;
import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.CategoryDAO;

/**
 * Created by User on 30.11.2015.
 */
public abstract class DAOFactory {
    public static final int MY_SQL = 1;

    public abstract UserDAO getUserDao();
    public abstract TestDAO getTestDao();
    public abstract QuestionDAO getQuestionDao();
    public abstract AnswerDAO getAnswerDao();
    public abstract CategoryDAO getCategoryDao();

    public static DAOFactory getDAOFactory(int factory) {
        switch (factory) {
            case MY_SQL:
                return MySQLDAOFactory.getInstance();
            default:
                return null;
        }
    }
}
