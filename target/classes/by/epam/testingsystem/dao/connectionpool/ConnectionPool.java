package by.epam.testingsystem.dao.connectionpool;

import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;

import java.sql.Connection;

/**
 * Created by User on 14.11.2015.
 */
public interface ConnectionPool {
    void initialize() throws ConnectionPoolException;
    void destroy();
    Connection takeConnection() throws ConnectionPoolException;
}
