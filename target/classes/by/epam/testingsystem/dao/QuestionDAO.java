package by.epam.testingsystem.dao;

import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.Question;

import java.util.List;

/**
 * Created by User on 14.11.2015.
 */
public interface QuestionDAO {
    List<Question> getQuestionList(int testId) throws DaoException;
    boolean addQuestion(int testId, String questionText, boolean oneCorrectAnswer) throws DaoException;
    int getQuestionId(int testId, String questionText) throws DaoException;
}
