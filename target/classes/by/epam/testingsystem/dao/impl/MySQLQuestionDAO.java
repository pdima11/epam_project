package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.QuestionDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.Question;
import by.epam.testingsystem.resource.Resource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class MySQLQuestionDAO implements QuestionDAO{
    private static final String MYSQL_GET_QUESTION_LIST = "mysql.get.question.list";
    private static final String MYSQL_ADD_QUESTION = "mysql.add.question";
    private static final String MYSQL_GET_QUESTION_ID = "mysql.get.question.id";
    private static final String QUESTION_ID_ATTRIBUTE = "question_id";
    private static final String QUESTION_ATTRIBUTE = "question";
    private static final String ONE_CORRECT_ANSWER_ATTRIBUTE = "one_correct_answer";

    private MySQLQuestionDAO() {
    }

    private static class SingletonHolder {
        private static final MySQLQuestionDAO INSTANCE = new MySQLQuestionDAO();
    }

    public static MySQLQuestionDAO getInstance() {
        return SingletonHolder.INSTANCE;
    }


    /**
     * This method gets questions from database for test with test ID <code>testId</code>.
     *
     * @param testId test ID
     * @return list of questions
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public List<Question> getQuestionList(int testId) throws DaoException{
        List<Question> questions = new ArrayList<>();
        String sqlQuery = Resource.valueOf(MYSQL_GET_QUESTION_LIST);

        try ( Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
              PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ){

            statement.setInt(1, testId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Question question = new Question();
                question.setId(resultSet.getInt(QUESTION_ID_ATTRIBUTE));
                question.setQuestion(resultSet.getString(QUESTION_ATTRIBUTE));
                question.setOneCorrectAnswer(resultSet.getBoolean(ONE_CORRECT_ANSWER_ATTRIBUTE));
                questions.add(question);
            }

            return questions;
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        }
    }


    /**
     * This method added new test to database.
     *
     * @param testId test ID
     * @param questionText question text
     * @param oneCorrectAnswer
     *        true if question has one correct answer,
     *        false if question has two and more correct answers
     * @return true if question has been successfully added to database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean addQuestion(int testId, String questionText, boolean oneCorrectAnswer) throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_ADD_QUESTION);

        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, questionText);
            statement.setBoolean(2, oneCorrectAnswer);
            statement.setInt(3, testId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }

    /**
     * This method gets question ID which is contained in test with ID <code>testId</code>.
     *
     * @param testId test ID
     * @return question ID if test with ID <code>testId</code> exists, otherwise -1
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public int getQuestionId(int testId, String questionText) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_GET_QUESTION_ID);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setInt(1, testId);
            statement.setString(2, questionText);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(QUESTION_ID_ATTRIBUTE);
            } else {
                return -1;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }
}
