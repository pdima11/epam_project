package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.CategoryDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.resource.Resource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by User on 22.11.2015.
 */
public class MySQLCategoryDAO implements CategoryDAO {
    private static final String MYSQL_CATEGORY_EXISTS = "mysql.category.exists";
    private static final String MYSQL_ADD_NEW_CATEGORY = "mysql.add.new.category";
    private static final String CATEGORY_ID_ATTRIBUTE = "category_id";

    private MySQLCategoryDAO() {
    }

    private static class SingletonHolder {
        private static final MySQLCategoryDAO INSTANCE = new MySQLCategoryDAO();
    }

    public static MySQLCategoryDAO getInstance() {
        return SingletonHolder.INSTANCE;
    }


    /**
     * This method added new category to database.
     *
     * @param category name of category
     * @return true if category has been successfully added to database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean addCategory(String category) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_ADD_NEW_CATEGORY);

        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, category);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }


    /**
     * This method gets category ID that has name <code>category</code>.
     *
     * @param category name of category
     * @return category ID if category with name <code>category</code> exists, otherwise -1
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public int getCategoryId(String category) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_CATEGORY_EXISTS);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, category);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(CATEGORY_ID_ATTRIBUTE);
            } else {
                return -1;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }
}
