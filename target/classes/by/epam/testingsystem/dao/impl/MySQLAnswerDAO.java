package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.AnswerDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.Answer;
import by.epam.testingsystem.resource.Resource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class MySQLAnswerDAO implements AnswerDAO{
    private static final String MYSQL_GET_ANSWER_LIST = "mysql.get.answer.list";
    private static final String MYSQL_ADD_ANSWER = "mysql.add.answer";
    private static final String ANSWER_ID_ATTRIBUTE = "answer_id";
    private static final String ANSWER_ATTRIBUTE = "answer";
    private static final String CORRECT_ATTRIBUTE = "correct";

    private MySQLAnswerDAO() {
    }

    private static class SingletonHolder {
        private static final MySQLAnswerDAO INSTANCE = new MySQLAnswerDAO();
    }

    public static MySQLAnswerDAO getInstance() {
        return SingletonHolder.INSTANCE;
    }


    /**
     * This method gets answers from database for question with question ID <code>questionId</code>.
     *
     * @param questionId question ID
     * @return list of answers
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public List<Answer> getAnswerList(int questionId) throws DaoException{
        List<Answer> answers = new ArrayList<>();
        String sqlQuery = Resource.valueOf(MYSQL_GET_ANSWER_LIST);

        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ) {

            statement.setInt(1, questionId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Answer answer = new Answer();
                answer.setId(resultSet.getInt(ANSWER_ID_ATTRIBUTE));
                answer.setAnswer(resultSet.getString(ANSWER_ATTRIBUTE));
                answer.setCorrect(resultSet.getBoolean(CORRECT_ATTRIBUTE) );
                answers.add(answer);
            }

            return answers;
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        }
    }


    /**
     * This method added new test to database.
     *
     * @param questionId question ID
     * @param answerText answer text
     * @param correct define whether answer correct or no
     * @return true if answer has been successfully added to database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean addAnswer(int questionId, String answerText, boolean correct) throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_ADD_ANSWER);

        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, answerText);
            statement.setBoolean(2, correct);
            statement.setInt(3, questionId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }

}
