package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.resource.Resource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class MySQLTestDAO implements TestDAO{
    private static final String MYSQL_GET_TEST_LIST = "mysql.get.test.list";
    private static final String MYSQL_GET_TEST_BY_ID = "mysql.get.test.by.id";
    private static final String MYSQL_GET_TESTS_NUMBER = "mysql.get.tests.number";
    private static final String MYSQL_ADD_NEW_TEST = "mysql.add.new.test";
    private static final String MYSQL_UPDATE_TEST = "mysql.update.test";
    private static final String MYSQL_GET_TEST_ID = "mysql.get.test.id";
    private static final String MYSQL_DELETE_TEST = "mysql.delete.test";
    private static final String MYSQL_TEST_NAME_EXISTS = "mysql.test.name.exists";
    private static final String TEST_ID_ATTRIBUTE = "test_id";
    private static final String TEST_NAME_ATTRIBUTE = "test_name";
    private static final String CATEGORY_NAME_ATTRIBUTE = "category_name";
    private static final String QUESTIONS_NUMBER_ATTRIBUTE = "questions_number";
    private static final String TESTS_NUMBER_ATTRIBUTE = "tests_number";
    private static final String TEST_GROUP_UPDATE_ID_ATTRIBUTE = "group_update_id";

    private MySQLTestDAO(){
    }

    private static class SingletonHolder {
        private static final MySQLTestDAO INSTANCE = new MySQLTestDAO();
    }

    public static MySQLTestDAO getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * This method finds test in database by id.
     *
     * @param testId test's ID
     * @return test
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public Test getTest(int testId) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_GET_TEST_BY_ID);
        try ( Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
              PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ) {

            statement.setInt(1, testId);
            ResultSet resultSet = statement.executeQuery();

            Test test = new Test();

            if (resultSet.next()) {
                test.setId(resultSet.getInt(TEST_ID_ATTRIBUTE));
                test.setGroupUpdateId(resultSet.getInt(TEST_GROUP_UPDATE_ID_ATTRIBUTE));
                test.setName(resultSet.getString(TEST_NAME_ATTRIBUTE));
                test.setCategory(resultSet.getString(CATEGORY_NAME_ATTRIBUTE));
                test.setQuestionsNumber(resultSet.getInt(QUESTIONS_NUMBER_ATTRIBUTE));
            }

            return test;
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        }
    }

    /**
     * This method gets tests from database
     * starting at position <code>offset</code>
     * in number of <code>testsNumberPerPage</code>.
     *
     * @param offset start position in database
     * @param testsNumberPerPage number of test
     * @return list of tests
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public List<Test> getTestList(int offset, int testsNumberPerPage) throws DaoException {
        List<Test> tests = new ArrayList<>();
        String sqlQuery = Resource.valueOf(MYSQL_GET_TEST_LIST);
        try ( Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
              PreparedStatement statement = connection.prepareStatement(sqlQuery)
            ) {

            statement.setInt(1, offset);
            statement.setInt(2, testsNumberPerPage);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Test test = new Test();
                test.setId(resultSet.getInt(TEST_ID_ATTRIBUTE));
                test.setGroupUpdateId(resultSet.getInt(TEST_GROUP_UPDATE_ID_ATTRIBUTE));
                test.setName(resultSet.getString(TEST_NAME_ATTRIBUTE));
                test.setCategory(resultSet.getString(CATEGORY_NAME_ATTRIBUTE));
                test.setQuestionsNumber(resultSet.getInt(QUESTIONS_NUMBER_ATTRIBUTE));
                tests.add(test);
            }

            return tests;
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException from dao ", e);
        }
    }

    /**
     * This method returns the number of tests that stored in the database.
     *
     * @return number of tests
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public int getTestsNumber() throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_GET_TESTS_NUMBER);
        int testsNumber = 0;
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                testsNumber = resultSet.getInt(TESTS_NUMBER_ATTRIBUTE);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }

        return testsNumber;
    }

    /**
     * This method added new test to database.
     *
     * @param testName name of new test
     * @param categoryId test's category ID
     * @return true if test has been successfully added to database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean addTest(String testName, int categoryId) throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_ADD_NEW_TEST);

        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, testName);
            statement.setInt(2, categoryId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }


    /**
     * This method added new test to database.
     *
     * @param testName name of new test
     * @param categoryId test's category ID
     * @return true if test has been successfully added to database, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean updateTest(String testName, int categoryId, int groupUpdateId) throws DaoException{
        String sqlQuery = Resource.valueOf(MYSQL_UPDATE_TEST);

        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setInt(1, groupUpdateId);
            statement.setString(2, testName);
            statement.setInt(3, categoryId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }


    /**
     * This method gets test ID that has name <code>testName</code>.
     *
     * @param testName name of test
     * @return test ID if test with name <code>testName</code> exists, otherwise -1
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public int getTestId(String testName) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_GET_TEST_ID);
        try (Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
             PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, testName);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(TEST_ID_ATTRIBUTE);
            } else {
                return -1;
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }


    /**
     *  This method deletes test from database by id.
     *
     * @param testId a test ID
     * @return true if test has been successfully deleted from database, otherwise false
     * @throws DaoException
     */
    @Override
    public boolean deleteTest(int testId) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_DELETE_TEST);
        try ( Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
              PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setInt(1, testId);
            int result = statement.executeUpdate();

            if (result > 0) {
                return true;
            } else {
                return false;
            }

        }  catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }


    /**
     * This method checks whether test with such <code>testName</code> exists in the database.
     *
     * @param testName name of test
     * @return true if test with this name exists, otherwise false
     * @throws DaoException if SQLException or ConnectionPoolException
     */
    @Override
    public boolean testNameExists(String testName) throws DaoException {
        String sqlQuery = Resource.valueOf(MYSQL_TEST_NAME_EXISTS);
        try ( Connection connection = ConnectionPoolImpl.getInstance().takeConnection();
              PreparedStatement statement = connection.prepareStatement(sqlQuery)
        ) {
            statement.setString(1, testName);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }

        }  catch (SQLException e) {
            throw new DaoException("SQLException from dao", e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("An error occurred while creating connection pool", e);
        }
    }

}
