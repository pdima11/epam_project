package by.epam.testingsystem.util;

import by.epam.testingsystem.domain.Answer;
import by.epam.testingsystem.domain.Question;
import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;

import java.util.List;

/**
 * Created by User on 22.11.2015.
 */
public class TestValidator {

    public static boolean isTestValid(Test test){
        boolean isTestNameValid = checkTestName(test.getName());
        boolean isTestCategoryValid = checkTestCategory(test.getCategory());
        boolean isQuestionsValid = checkQuestions(test.getQuestions());

        if (isTestNameValid  && isTestCategoryValid && isQuestionsValid) {
            return true;
        } else {
            return false;
        }

    }

    private static boolean checkTestName(String testName) {
        if (testName != null) {
            if (!testName.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkTestCategory(String category) {
        if (category != null) {
            if (!category.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkQuestions(List<Question> questions) {
        if (questions != null) {
            if (questions.size() == 0) {
                return false;
            }
        } else {
            return false;
        }

        for (Question question: questions) {
            String questionText = question.getQuestion();

            if (questionText != null) {
                if (questionText.isEmpty()) {
                    return false;
                }
            } else {
                return false;
            }

            boolean isAnswersValid = checkAnswers(question.getAnswers());

            if (!isAnswersValid) {
                return false;
            }
        }

        return true;
    }

    private static boolean checkAnswers(List<Answer> answers) {
        if (answers != null) {
            if (answers.size() < 2) {
                return false;
            }
        } else {
            return false;
        }

        boolean isCorrectAnswerExist = false;

        for (Answer answer: answers) {
            String answerText = answer.getAnswer();

            if (answerText != null) {
                if (answerText.isEmpty()) {
                    return false;
                }
            } else {
                return false;
            }

            if (answer.isCorrect()) {
                isCorrectAnswerExist = true;
            }

        }

        return isCorrectAnswerExist;
    }
}
