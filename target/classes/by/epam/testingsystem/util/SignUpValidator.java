package by.epam.testingsystem.util;

import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;

/**
 * Created by User on 14.11.2015.
 */
public class SignUpValidator {
    private static final String LOGIN_PATTERN = "pattern.login";
    private static final String EMAIL_PATTERN = "pattern.email";
    private static final String PASSWORD_PATTERN = "pattern.password";

    public static boolean isSignUpValid(User user, String confirmPassword){
        boolean isLoginValid = checkLogin(user.getLogin());
        boolean isEmailValid = checkEmail(user.getEmail());
        boolean isPasswordValid = checkPassword(user.getPassword());
        boolean isConfirmPasswordValid = checkConfirmPassword(confirmPassword, user.getPassword());

        if (isLoginValid  && isEmailValid && isPasswordValid && isConfirmPasswordValid) {
            return true;
        } else {
            return false;
        }

    }

    private static boolean checkLogin(String login) {
        if (login != null) {
            if (login.matches(Resource.valueOf(LOGIN_PATTERN))) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkEmail(String email) {
        if (email != null) {
            if (email.matches(Resource.valueOf(EMAIL_PATTERN))) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkPassword(String password) {
        if (password != null) {
            if (password.matches(Resource.valueOf(PASSWORD_PATTERN))) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkConfirmPassword(String confirmPassword, String password) {
        if (confirmPassword != null) {
            if (confirmPassword.equals(password)) {
                return true;
            }
        }
        return false;
    }
}
