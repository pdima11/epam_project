package by.epam.testingsystem.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class Question {
    private int id;
    private String question;
    private boolean oneCorrectAnswer;
    private List<Answer> answers = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isOneCorrectAnswer() {
        return oneCorrectAnswer;
    }

    public void setOneCorrectAnswer(boolean oneCorrectAnswer) {
        this.oneCorrectAnswer = oneCorrectAnswer;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
