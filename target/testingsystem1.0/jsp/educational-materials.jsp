<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 24.11.2015
  Time: 13:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
  <title><tag:resource key="jsp.title.menu.educational.materials"/></title>
</head>
<body>
  <jsp:include page="template/headerTemplate.jsp"/>
  <div class="container">
    <div class="row">
      <div style="margin-top: 10px" class="col-md-8 col-md-offset-2">
        <tag:resource key="jsp.title.educational.materials"/>
      </div>
    </div>
  </div>
</body>
</html>
