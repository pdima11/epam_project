<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 12.11.2015
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<html>
  <head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/starttest.css"/>
  </head>
  <body>
    <div>
      <jsp:include page="template/headerTemplate.jsp"/>
    </div>


    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;">
          <h4>${test.name}</h4>
          <b><tag:resource key="jsp.title.test.category"/>:</b> ${test.category}
          <br/>
          <b><tag:resource key="jsp.title.test.questions.number"/>:</b> ${test.questionsNumber}
          <br/>
          <c:set var="questionNumber" value="0"/>
          <c:forEach var="question" items="${test.questions}">
            <div class="panel panel-danger panel-pricing">

              <div class="panel-body">
                <p>
                  ${question.question}
                </p>
              </div>

                <c:if test="${question.oneCorrectAnswer == true}">
                  <c:forEach var="answer" items="${question.answers}">
                    <div class="funkyradio">
                      <div class="funkyradio-primary">
                        <input type="radio" name="answer-group${questionNumber}" id="answer${answer.id}" value="${answer.id}" />
                        <label for="answer${answer.id}">${answer.answer}</label>
                      </div>
                    </div>
                  </c:forEach>
                </c:if>
                <c:if test="${question.oneCorrectAnswer == false}">
                  <c:forEach var="answer" items="${question.answers}">
                    <div class="funkyradio">
                      <div class="funkyradio-primary">
                        <input type="checkbox" name="answer-group${questionNumber}" id="answer${answer.id}" value="${answer.id}" />
                        <label for="answer${answer.id}">${answer.answer}</label>
                      </div>
                    </div>
                  </c:forEach>
                </c:if>
                <c:set var="questionNumber" value="${questionNumber+1}"/>

            </div>
          </c:forEach>

        </div>
      </div>

      <br/>

      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <form action="${pageContext.request.contextPath}/TestingSystem" method="post">
            <c:forEach var="i" begin="0" end="${questionNumber-1}">
              <input type="hidden" name="user-answers-id${i}"/>
            </c:forEach>
            <input type="hidden" name="questions-number" value="${test.questionsNumber}"/>
            <input type="hidden" name="test-id" value="${test.id}"/>
            <input type="hidden" name="command" value="testResult"/>
            <button type="button" onclick="getAnswersId(this.form, ${questionNumber})" class="btn btn-primary">
              <span class="glyphicon glyphicon-check"></span> <tag:resource key="jsp.title.test.check"/>
            </button>
          </form>

        </div>
      </div>

      <br/>

    </div>

  </body>
</html>

<script type="text/javascript">
  function getAnswersId(form, questionNumber) {
    for (var i = 0; i < questionNumber; i++) {
      var question = "answer-group" + i; // name for question number i
      var answer = "user-answers-id" + i;
      var element = document.getElementsByName(question);

      for (var j = 0; j < element.length; j++) {
        if (element[j].checked) {
          document.getElementsByName(answer)[0].value += element[j].value + "_";
          console.log(document.getElementsByName(answer)[0].value);
        }
      }

    }
    form.submit();
  }
</script>