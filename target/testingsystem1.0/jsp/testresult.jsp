<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 13.11.2015
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
  <div>
    <jsp:include page="template/headerTemplate.jsp"/>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3" style="margin-top: 20px; text-align: center;">
        <img src="../images/test_result.png">

        <h1 style="margin-top: 5px;"><tag:resource key="jsp.title.test.result.score"/> ${percent}%</h1>
      </div>

      <div class="col-md-6 col-md-offset-4" style="margin-top: 10px;">
        <h3><tag:resource key="jsp.title.test.result.total"/>: ${testQuestionsNumber}</h3>
        <h3><tag:resource key="jsp.title.test.result.attempted"/>: ${attempted}</h3>
        <h3><tag:resource key="jsp.title.test.result.correct"/>: ${correctAnswersNumber}</h3>
      </div>

      <div class="col-md-6 col-md-offset-3" style="margin-top: 20px; margin-bottom: 20px; text-align: center;">
        <a href="${pageContext.request.contextPath}/TestingSystem?command=loadTestList" class="btn btn-default">
          <i class="glyphicon glyphicon-chevron-right"></i>
          <tag:resource key="jsp.button.new.test"/>
        </a>
      </div>

    </div>
  </div>
</body>
</html>
