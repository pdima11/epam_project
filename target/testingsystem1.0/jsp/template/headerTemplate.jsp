<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="resource" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/blog.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>

<div class="blog-masthead">
    <div class="container-fluid">
        <nav class="blog-nav navbar-right">
            <div class="blog-nav-item">
                <jsp:include page="langTemplate.jsp" />
            </div>
            <c:if test="${user == null}">
                <a class="blog-nav-item" href="${pageContext.request.contextPath}/jsp/authorization/registration.jsp">
                    <span class="glyphicon glyphicon-user"></span>
                    <tag:resource key="jsp.link.signup" />
                </a>
                <a class="blog-nav-item" href="${pageContext.request.contextPath}/jsp/authorization/authorization.jsp">
                    <span class="glyphicon glyphicon-log-in"></span>
                    <tag:resource key="jsp.link.login" />
                </a>
            </c:if>
            <c:if test="${user != null}">
                <span class="blog-nav-item"><tag:resource key="jsp.title.welcome"/>, ${user.login} </span>
                <a class="blog-nav-item" href="${pageContext.request.contextPath}/TestingSystem?command=logOut">
                    <span class="glyphicon glyphicon-log-out"></span>
                    <tag:resource key="jsp.link.logout" />
                </a>
            </c:if>
        </nav>
    </div>
</div>


<div class="navbar navbar-inverse1 set-radius-zero">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/jsp/index.jsp">
                <span class="navbar-brand" style="color: white; font-size: 30px;"><tag:resource key="jsp.title.project"/></span>
            </a>
        </div>
        <div class="left-div">
            <div class="user-settings-wrapper">
                <ul class="nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <span class="glyphicon glyphicon-list-alt test-span"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="menu-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <div id="select-menu">
                    <ul id="menu-top" class="nav navbar-nav navbar-right">
                        <li><a id="home-page" class="menu-top-active"  href="${pageContext.request.contextPath}/jsp/index.jsp"><tag:resource key="jsp.title.menu.home"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/TestingSystem?command=loadTestList"><tag:resource key="jsp.title.menu.test.list"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/jsp/educational-materials.jsp"><tag:resource key="jsp.title.menu.educational.materials"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/jsp/about.jsp"><tag:resource key="jsp.title.menu.about"/></a></li>
                        <c:if test="${user.role == 'Admin'}">
                            <li><a href="${pageContext.request.contextPath}/TestingSystem?command=loadUserList"><tag:resource key="jsp.title.menu.admin"/></a></li>
                        </c:if>
                        <c:if test="${user.role == 'Tutor'}">
                            <li><a href="${pageContext.request.contextPath}/tutormenu"><tag:resource key="jsp.title.menu.tutor"/></a></li>
                        </c:if>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.nav a').filter(function() {
            $('.nav').find('.menu-top-active').removeClass('menu-top-active');
            return this.href == window.location;
        }).parent().addClass('menu-top-active');
    });
</script>
