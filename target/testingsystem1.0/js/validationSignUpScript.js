/**
 * Created by User on 08.11.2015.
 */
function validateRegEx(regex, input, helpText) {
    if (!regex.test(input)) {
        if (helpText != null)
            helpText.style.display = "block";
        return false;
    }
    else {
        if (helpText != null)
            helpText.style.display = "none";
        return true;
    }
}

function validatePasswordLength(inputField, helpText) {
    return validateRegEx(new RegExp("^.{" + 6 + "," + 32 + "}$"),
        inputField.value, helpText);
}

function validateLogin(inputField, helpText) {
    return validateRegEx(/[A-z0-9]{3,}/,
        inputField.value, helpText);
}

function validateEmail(inputField, helpText) {
    return validateRegEx(/^[\w\.-_\+]+@[\w-]+(\.\w{2,3})+$/,
        inputField.value, helpText);
}

function validateConfirmPassword(inputField, passwordField, helpText) {
    if (passwordField.value > 0) {
        if (validatePasswordLength(inputField, helpText)) {
            if (inputField.value == passwordField.value) {
                if (helpText != null){
                    helpText.style.display = "none";
                }
                return true;
            } else {
                if (helpText != null){
                    helpText.style.display = "block";
                }
                return false;
            }
        } else {
            if (helpText != null){
                helpText.style.display = "block";
            }
            return false;
        }
    }
}

function checkSignUp(form) {
    if (validatePasswordLength(form["password"], form["password-help"]) &&
        validateConfirmPassword(form["confirm-password"], form["password"], form["confirm-password-help"]) &&
        validateLogin(form["login"], form["login-help"]) &&
        validateEmail(form["email"], form["email-help"])) {
        form.submit();
    } else {
        alert(document.getElementById('incorrectly-form').innerHTML);
    }
}

function success(idName){
    $(idName).removeClass('has-error');
    $(idName).addClass('has-success');
}

function error(idName) {
    $(idName).removeClass('has-success');
    $(idName).addClass('has-error');
}

$(function() {
    $("#login-input input").blur(function(e) {
        e.preventDefault();
        if (validateLogin(this, document.getElementById('login-help'))){
            success("#login-input");
            $("#login-input").addClass('margin-bottom-20');
        } else {
            error("#login-input");
            $("#login-input").removeClass('margin-bottom-20');
        }
    });
});

$(function() {
    $("#email-input input").blur(function(e) {
        e.preventDefault();
        if (validateEmail(this, document.getElementById('email-help'))){
            success("#email-input");
            $("#email-input").addClass('margin-bottom-20');
        } else {
            error("#email-input");
            $("#email-input").removeClass('margin-bottom-20');
        }
    });
});

$(function() {
    $("#password-input input").blur(function(e) {
        e.preventDefault();
        if (validatePasswordLength(this, document.getElementById('password-help'))){
            success("#password-input");
            $("#password-input").addClass('margin-bottom-20');
        } else {
            error("#password-input");
            $("#password-input").removeClass('margin-bottom-20');
        }
    });
});

$(function() {
    $("#confirm-password-input input").blur(function(e) {
        e.preventDefault();
        if (validateConfirmPassword(this, document.getElementById('password'), document.getElementById('confirm-password-help'))){
            success("#confirm-password-input");
            $("#confirm-password-input").addClass('margin-bottom-20');
        } else {
            error("#confirm-password-input");
            $("#confirm-password-input").removeClass('margin-bottom-20');
        }
    });
});