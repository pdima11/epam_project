package by.epam.testingsystem.dao;

import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.domain.Answer;

import java.util.List;


/**
 * Created by User on 14.11.2015.
 */
public interface AnswerDAO {
    List<Answer> getAnswerList(int questionId) throws DaoException;
    boolean addAnswer(int questionId, String answerText, boolean correct) throws DaoException;
}
