package by.epam.testingsystem.dao.exception;

import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 03.11.2015.
 */
public class DaoException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Exception e) {
        super(message, e);
    }
}
