package by.epam.testingsystem.util;

import by.epam.testingsystem.resource.Resource;

/**
 * Created by User on 26.11.2015.
 */
public class RequestParameterValidator {
    private static final String INTEGER_PATTERN = "pattern.integer";

    public static boolean isInteger(String requestParameter){
        boolean isNotEmpty = false;
        boolean isInteger = false;

        if (requestParameter != null) {
            isNotEmpty = !(requestParameter.isEmpty());
            isInteger = requestParameter.matches(Resource.valueOf(INTEGER_PATTERN));
        }

        if (isNotEmpty && isInteger) {
            return true;
        } else {
            return false;
        }

    }


}
