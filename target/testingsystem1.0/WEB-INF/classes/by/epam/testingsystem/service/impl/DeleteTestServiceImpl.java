package by.epam.testingsystem.service.impl;

import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.exception.DaoException;
import by.epam.testingsystem.dao.factory.DAOFactory;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.service.DeleteTestService;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 23.11.2015.
 */
public class DeleteTestServiceImpl implements DeleteTestService{
    private DeleteTestServiceImpl(){
    }

    private static class SingletonHolder {
        private static final DeleteTestServiceImpl INSTANCE = new DeleteTestServiceImpl();
    }

    public static DeleteTestServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public boolean deleteTest(int testId) throws ServiceException {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MY_SQL);

        if (daoFactory == null) {
            throw new ServiceException("DAOFactory returned null");
        }

        TestDAO testDAO = daoFactory.getTestDao();

        boolean testDeleted = testDAO.deleteTest(testId);

        return testDeleted;
    }
}
