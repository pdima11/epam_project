package by.epam.testingsystem.service;

import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 15.11.2015.
 */
public interface TestResultService {
    int getCorrectUserAnswersNumber(int testId, String[] userAnswers) throws ServiceException;
    int getPercent(int correctAnswerNumber, int questionsNumber) throws ServiceException;
    int getAnsweredQuestionsNumber(String[] userAnswers) throws ServiceException;
 }
