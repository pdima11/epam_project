package by.epam.testingsystem.service;

import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 11.11.2015.
 */
public interface DeleteUserService {
    boolean deleteUser(int userId) throws ServiceException;
}
