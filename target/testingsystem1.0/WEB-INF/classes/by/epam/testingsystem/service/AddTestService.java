package by.epam.testingsystem.service;

import by.epam.testingsystem.domain.Test;
import by.epam.testingsystem.service.exception.ServiceException;

/**
 * Created by User on 22.11.2015.
 */
public interface AddTestService {
    boolean addTest(Test test) throws ServiceException;
}
