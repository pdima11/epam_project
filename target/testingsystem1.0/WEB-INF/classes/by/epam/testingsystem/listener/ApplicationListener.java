package by.epam.testingsystem.listener;

import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by User on 13.11.2015.
 */
public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        ConnectionPoolImpl connectionPoolImpl = ConnectionPoolImpl.getInstance();
        connectionPoolImpl.destroy();
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        ConnectionPoolImpl connectionPoolImpl = ConnectionPoolImpl.getInstance();
        try {
            connectionPoolImpl.initialize();
        } catch (ConnectionPoolException e){
            e.printStackTrace();
        }
    }
}
