package by.epam.testingsystem.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.11.2015.
 */
public class Test {
    private int id;
    private int groupUpdateId;
    private String name;
    private String category;
    private int questionsNumber;
    private List<Question> questions = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupUpdateId() {
        return groupUpdateId;
    }

    public void setGroupUpdateId(int groupUpdateId) {
        this.groupUpdateId = groupUpdateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getQuestionsNumber() {
        return questionsNumber;
    }

    public void setQuestionsNumber(int questionsNumber) {
        this.questionsNumber = questionsNumber;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
