package by.epam.testingsystem.controller.command.impl.admin;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.UserListService;
import by.epam.testingsystem.service.impl.UserListServiceImpl;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by User on 10.11.2015.
 */
public class LoadUserListCommand implements Command {
    private static final String FORWARD_PAGE_USER_LIST = "forward.page.admin.menu";
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String USER_LIST_PARAMETER = "users";
    private static final String CURRENT_PAGE_PARAMETER = "currentPage";
    private static final String PAGES_NUMBER_PARAMETER = "pagesNumber";
    private static final String PAGE_PARAMETER = "page";
    private static final String USER_PARAMETER = "user";
    private static final String ROLE_PARAMETER = "Admin";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        String forwardPage = Resource.valueOf(FORWARD_PAGE_USER_LIST);

        HttpSession session = request.getSession(false);

        if (session == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        User currentUser = (User) session.getAttribute(USER_PARAMETER);
        if (currentUser == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        if (!ROLE_PARAMETER.equals(currentUser.getRole())) {
            return Resource.valueOf(FORWARD_PAGE_HOME);
        }

        int page = 1;
        String chosenPageParameter = request.getParameter(PAGE_PARAMETER);
        boolean isChosenPageParameterInteger = RequestParameterValidator.isInteger(chosenPageParameter);

        if (isChosenPageParameterInteger) {
            page = Integer.valueOf(chosenPageParameter);
        }

        UserListService userListService = UserListServiceImpl.getInstance();
        try {
            List<User> users = userListService.getUserList(page);
            int pagesNumber = userListService.getPagesNumber();

            request.setAttribute(USER_LIST_PARAMETER, users);
            request.setAttribute(CURRENT_PAGE_PARAMETER, page);
            request.setAttribute(PAGES_NUMBER_PARAMETER, pagesNumber);

            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing LoginServiceImpl", e);
        }
    }
}
