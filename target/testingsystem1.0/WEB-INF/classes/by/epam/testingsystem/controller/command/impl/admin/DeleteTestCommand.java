package by.epam.testingsystem.controller.command.impl.admin;

import by.epam.testingsystem.controller.command.Command;
import by.epam.testingsystem.controller.command.exception.CommandException;
import by.epam.testingsystem.domain.User;
import by.epam.testingsystem.resource.Resource;
import by.epam.testingsystem.service.DeleteTestService;
import by.epam.testingsystem.service.exception.ServiceException;
import by.epam.testingsystem.service.impl.DeleteTestServiceImpl;
import by.epam.testingsystem.util.RequestParameterValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by User on 23.11.2015.
 */
public class DeleteTestCommand implements Command {
    private static final String FORWARD_PAGE_AUTHORIZATION = "forward.page.authorization";
    private static final String FORWARD_PAGE_DELETE_TEST = "forward.page.load.tests";
    private static final String FORWARD_PAGE_HOME = "forward.page.home";
    private static final String SUCCESSFUL_DELETE_TEST = "testRemoved";
    private static final String TEST_ID_PARAMETER = "test-id";
    private static final String USER_PARAMETER = "user";
    private static final String ROLE_PARAMETER = "Admin";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = Resource.valueOf(FORWARD_PAGE_DELETE_TEST);

        HttpSession session = request.getSession(false);

        if (session == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        User currentUser = (User) session.getAttribute(USER_PARAMETER);
        if (currentUser == null) {
            return Resource.valueOf(FORWARD_PAGE_AUTHORIZATION);
        }
        if (!ROLE_PARAMETER.equals(currentUser.getRole())) {
            return Resource.valueOf(FORWARD_PAGE_HOME);
        }

        try {
            String testIdParameter = request.getParameter(TEST_ID_PARAMETER);
            boolean deleteTest = false;
            boolean isTestIdParameterInteger = RequestParameterValidator.isInteger(testIdParameter);

            if (isTestIdParameterInteger) {
                int testId = Integer.valueOf(testIdParameter);
                DeleteTestService deleteTestService = DeleteTestServiceImpl.getInstance();
                deleteTest = deleteTestService.deleteTest(testId);
            } else {
                forwardPage = Resource.valueOf(FORWARD_PAGE_HOME);
            }

            if (deleteTest){
                request.setAttribute(SUCCESSFUL_DELETE_TEST, true);
            } else {
                request.setAttribute(SUCCESSFUL_DELETE_TEST, false);
            }

            return forwardPage;
        } catch (ServiceException e) {
            throw new CommandException("Error while executing DeleteTestService", e);
        }
    }

}
