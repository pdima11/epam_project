<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.11.2015
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/starttest.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/blog.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/tutorMenuScript.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
 <div>
     <div class="blog-masthead">
         <div class="container-fluid">
             <nav class="blog-nav navbar-right">
                 <div class="blog-nav-item">
                     <div id="lang-selection" class="btn-group language_selector">
                         <button type="button" class="btn btn-default dropdown-toggle mouse-leave" data-toggle="dropdown">
                             <img src="<tag:resource key="jsp.select.language" />">
                         </button>
                         <ul class="dropdown-menu">
                             <li style="text-align: center; cursor: pointer">
                                 <img src="<tag:resource key="jsp.select.language" />">
                             </li>
                             <c:if test="${language == 'en'}">
                                 <li style="text-align: center; cursor: pointer">
                                     <a href="${pageContext.request.contextPath}/TestingSystem?language=ru&command=changeLanguage">
                                         <img src="<tag:resource key="jsp.not.select.language" />">
                                     </a>
                                 </li>
                             </c:if>
                             <c:if test="${language == 'ru'}">
                                 <li style="text-align: center; cursor: pointer">
                                     <a href="${pageContext.request.contextPath}/TestingSystem?language=en&command=changeLanguage">
                                         <img src="<tag:resource key="jsp.not.select.language" />">
                                     </a>
                                 </li>
                             </c:if>
                         </ul>
                     </div>
                 </div>
                 <c:if test="${user == null}">
                     <a class="blog-nav-item" href="${pageContext.request.contextPath}/jsp/authorization/registration.jsp">
                         <span class="glyphicon glyphicon-user"></span>
                         <tag:resource key="jsp.link.signup" />
                     </a>
                     <a class="blog-nav-item" href="${pageContext.request.contextPath}/jsp/authorization/authorization.jsp">
                         <span class="glyphicon glyphicon-log-in"></span>
                         <tag:resource key="jsp.link.login" />
                     </a>
                 </c:if>
                 <c:if test="${user != null}">
                     <span class="blog-nav-item"><tag:resource key="jsp.title.welcome"/>, ${user.login} </span>
                     <a class="blog-nav-item" href="${pageContext.request.contextPath}/TestingSystem?command=logOut">
                         <span class="glyphicon glyphicon-log-out"></span>
                         <tag:resource key="jsp.link.logout" />
                     </a>
                 </c:if>
             </nav>
         </div>
     </div>


     <div class="navbar navbar-inverse1 set-radius-zero">
         <div class="container">
             <div class="navbar-header">
                 <a class="navbar-brand" href="${pageContext.request.contextPath}/jsp/index.jsp">
                     <span class="navbar-brand" style="color: white; font-size: 30px;"><tag:resource key="jsp.title.project"/></span>
                 </a>
             </div>
             <div class="left-div">
                 <div class="user-settings-wrapper">
                     <ul class="nav">
                         <li class="dropdown">
                             <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                 <span class="glyphicon glyphicon-list-alt test-span"></span>
                             </a>
                         </li>
                     </ul>
                 </div>
             </div>
         </div>
     </div>

     <section class="menu-section">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="navbar-collapse collapse ">
                         <div id="select-menu">
                             <ul id="menu-top" class="nav navbar-nav navbar-right">
                                 <li><a id="home-page" class="menu-top-active"  href="${pageContext.request.contextPath}/jsp/index.jsp"><tag:resource key="jsp.title.menu.home"/></a></li>
                                 <li><a href="${pageContext.request.contextPath}/TestingSystem?command=loadTestList"><tag:resource key="jsp.title.menu.test.list"/></a></li>
                                 <li><a href="${pageContext.request.contextPath}/jsp/educational-materials.jsp"><tag:resource key="jsp.title.menu.educational.materials"/></a></li>
                                 <li><a href="${pageContext.request.contextPath}/jsp/about.jsp"><tag:resource key="jsp.title.menu.about"/></a></li>
                                 <c:if test="${user.role == 'Admin'}">
                                     <li><a href="${pageContext.request.contextPath}/TestingSystem?command=loadUserList"><tag:resource key="jsp.title.menu.admin"/></a></li>
                                 </c:if>
                                 <c:if test="${user.role == 'Tutor'}">
                                     <li><a href="${pageContext.request.contextPath}/tutormenu"><tag:resource key="jsp.title.menu.tutor"/></a></li>
                                 </c:if>
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
 </div>
<c:if test="${user.role == 'Tutor'}">
  <div style="margin-top: 50px;" class="container">
    <div class="row">
        <form action="${pageContext.request.contextPath}/TestingSystem" method="post">
            <div class="col-md-4 col-md-offset-2">
                <c:if test="${testAdded == true}">
                    <span style="color: #1da934;"><tag:resource key="jsp.message.test.added"/></span>
                </c:if>
                <c:if test="${testAdded == false}">
                    <span style="color: #a94442;"><tag:resource key="jsp.message.test.not.added"/></span>
                </c:if>
                <c:if test="${testNameExists == true}">
                    <span style="color: #a94442;"><tag:resource key="jsp.message.test.name.exists"/></span>
                </c:if>
                <div id="testname-input" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
                    <input type="text" name="test-name" id="test-name" class="form-control" autocomplete="off"
                           placeholder="<tag:resource key="jsp.title.test.name"/>"/>
                </div>

                <div id="category-input" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                    <input type="text" name="category" id="category" class="form-control" autocomplete="off"
                           placeholder="<tag:resource key="jsp.title.test.category"/>"/>
                </div>
            </div>

            <div id="questions" class="col-md-8 col-md-offset-2" style="margin-top: 40px">

            </div>

            <div class="col-md-2 col-md-offset-2" style="margin-bottom: 20px" >
                <button id="add-question" type="button" onclick="addQuestion('<tag:resource key="jsp.button.add.answer"/>')" class="btn btn-primary">
                    <span class="glyphicon glyphicon-plus"></span> <tag:resource key="jsp.button.add.question"/>
                </button>
            </div>
            <div class="col-md-2" style="margin-bottom: 20px">
                <button id="remove-question" style="visibility: hidden" type="button" onclick="removeQuestion()"
                        class="btn btn-danger">
                    <span class="glyphicon glyphicon-remove"></span> <tag:resource key="jsp.button.remove.question"/>
                </button>
            </div>

            <input type="hidden" name="questions-number" id="questions-number" value="0">
            <input type="hidden" name="command" value="addTest"/>
            <input style="visibility: hidden" id="add-test" type="button" onclick="addTest(this.form)"
                   class="btn btn-primary" value="<tag:resource key="jsp.button.add.test"/>"/>
        </form>
    </div>
  </div>
</c:if>
</body>
</html>

<script type="text/javascript">
    window.onload = init(0, 0);
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.nav a').filter(function() {
            $('.nav').find('.menu-top-active').removeClass('menu-top-active');
            return this.href == window.location;
        }).parent().addClass('menu-top-active');
    });
</script>