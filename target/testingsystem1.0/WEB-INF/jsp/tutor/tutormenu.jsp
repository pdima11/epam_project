<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.11.2015
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/starttest.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/tutorMenuScript.js"></script>
</head>
<body>
 <div>
     <jsp:include page="${pageContext.request.contextPath}/jsp/template/headerTemplate.jsp" flush="true"/>
 </div>
<c:if test="${user.role == 'Tutor'}">
  <div style="margin-top: 50px;" class="container">
    <div class="row">
        <form action="${pageContext.request.contextPath}/TestingSystem" method="post">
            <div class="col-md-4 col-md-offset-2">
                <c:if test="${testAdded == true}">
                    <span style="color: #1da934;"><tag:resource key="jsp.message.test.added"/></span>
                </c:if>
                <c:if test="${testAdded == false}">
                    <span style="color: #a94442;"><tag:resource key="jsp.message.test.not.added"/></span>
                </c:if>
                <c:if test="${testNameExists == true}">
                    <span style="color: #a94442;"><tag:resource key="jsp.message.test.name.exists"/></span>
                </c:if>
                <div id="testname-input" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
                    <input type="text" name="test-name" id="test-name" class="form-control" autocomplete="off"
                           placeholder="<tag:resource key="jsp.title.test.name"/>"/>
                </div>

                <div id="category-input" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                    <input type="text" name="category" id="category" class="form-control" autocomplete="off"
                           placeholder="<tag:resource key="jsp.title.test.category"/>"/>
                </div>
            </div>

            <div id="questions" class="col-md-8 col-md-offset-2" style="margin-top: 40px">

            </div>

            <div class="col-md-2 col-md-offset-2" style="margin-bottom: 20px" >
                <button id="add-question" type="button" onclick="addQuestion('<tag:resource key="jsp.button.add.answer"/>')" class="btn btn-primary">
                    <span class="glyphicon glyphicon-plus"></span> <tag:resource key="jsp.button.add.question"/>
                </button>
            </div>
            <div class="col-md-2" style="margin-bottom: 20px">
                <button id="remove-question" style="visibility: hidden" type="button" onclick="removeQuestion()"
                        class="btn btn-danger">
                    <span class="glyphicon glyphicon-remove"></span> <tag:resource key="jsp.button.remove.question"/>
                </button>
            </div>

            <input type="hidden" name="questions-number" id="questions-number" value="0">
            <input type="hidden" name="command" value="addTest"/>
            <input style="visibility: hidden" id="add-test" type="button" onclick="addTest(this.form)"
                   class="btn btn-primary" value="<tag:resource key="jsp.button.add.test"/>"/>
        </form>
    </div>
  </div>
</c:if>
</body>
</html>

<script type="text/javascript">
    window.onload = init(0, 0);
</script>