package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.TestDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPool;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by User on 24.11.2015.
 */
public class MySQLTestDAOTest {

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.initialize();
    }

    @Test
    public void testGetTest() throws Exception {
        MySQLDAOFactory daoFactory = MySQLDAOFactory.getInstance();
        TestDAO testDAO = daoFactory.getTestDao();

        int testId = 1;
        by.epam.testingsystem.domain.Test test = testDAO.getTest(testId);

        assertEquals(1, test.getId());
        assertEquals("Capitals of Europe", test.getName());
        assertEquals("Geography", test.getCategory());
        assertEquals(3, test.getQuestionsNumber());
    }

    @Test
    public void testGetTestId() throws Exception {
        MySQLDAOFactory daoFactory = MySQLDAOFactory.getInstance();
        TestDAO testDAO = daoFactory.getTestDao();

        int testId = 1;
        by.epam.testingsystem.domain.Test test = testDAO.getTest(testId);

        assertEquals(1, test.getId());
        assertEquals("Capitals of Europe", test.getName());
        assertEquals("Geography", test.getCategory());
        assertEquals(3, test.getQuestionsNumber());
    }

    @Test
    public void testTestNameExists() throws Exception {
        MySQLDAOFactory daoFactory = MySQLDAOFactory.getInstance();
        TestDAO testDAO = daoFactory.getTestDao();

        String testName = "Capitals of Europe";
        boolean result = testDAO.testNameExists(testName);

        assertEquals(true, result);
    }


}