package by.epam.testingsystem.dao.impl;

import by.epam.testingsystem.dao.UserDAO;
import by.epam.testingsystem.dao.connectionpool.ConnectionPool;
import by.epam.testingsystem.dao.connectionpool.ConnectionPoolImpl;
import by.epam.testingsystem.dao.connectionpool.exception.ConnectionPoolException;
import by.epam.testingsystem.dao.factory.MySQLDAOFactory;
import by.epam.testingsystem.domain.User;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by User on 24.11.2015.
 */
public class MySQLUserDAOTest {

    @BeforeClass
    public static void init() throws ConnectionPoolException{
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.initialize();
    }

    @Test
    public void testGetUser() throws Exception {
        MySQLDAOFactory daoFactory = MySQLDAOFactory.getInstance();
        UserDAO userDAO = daoFactory.getUserDao();

        String userLogin = "admin";
        String userPassword = "admin11";
        User testUser = userDAO.getUser(userLogin, userPassword);

        assertEquals(1, testUser.getId());
        assertEquals("admin", testUser.getLogin());
        assertEquals("admin11", testUser.getPassword());
        assertEquals("admin@adm.com", testUser.getEmail());
        assertEquals("Admin", testUser.getRole());
    }

    @Test
    public void testUserExists() throws Exception {
        MySQLDAOFactory daoFactory = MySQLDAOFactory.getInstance();
        UserDAO userDAO = daoFactory.getUserDao();

        String userLogin = "admin";
        boolean testResult = userDAO.userExists(userLogin);

        assertEquals(true, testResult);
    }
}